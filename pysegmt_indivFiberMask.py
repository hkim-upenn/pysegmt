from pysegmt_utils import *
import argparse

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Directory management
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
workDir = os.getcwd()
segDir = os.path.join(workDir,"segmentations")
tomoDir = os.path.join(workDir,"tomograms")
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Arguments
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
parser = argparse.ArgumentParser(description="From individual filament csv files, creates masks",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--suffix", help="suffix for output", default="")
args = parser.parse_args()
config = vars(args)
suffix =  config["suffix"]


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Get input tomograms and segmentations
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
tomoFiles = list(glob(os.path.join(tomoDir,"*_preproc.hdf")))
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Generate masked tomograms
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
for i in range(len(tomoFiles)):
    print("TOMOGRAM:\t"+tomoFiles[i])
    for j in range(2):
        print("Fiber %02d" % j)
        coordsCSV2Tomo(tomoFiles[i], idx=j, suffix=suffix)
    print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 