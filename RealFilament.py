## A single, real microtubule
# Comparing Method 1 to Method 2 on a single, real microtubule filament
from pysegmt import *

workDir="/ChangLab5-hd2/hannah/data/test/"
tomoFiles = list(glob(os.path.join(workDir,"tomograms","TS16_unblur_orig__bin4.hdf")))
segFiles = list(glob(os.path.join(workDir,"segmentations","*.pdb")))
tomoFile = tomoFiles[0]
segFile = segFiles[0]

n = 17
f = open("RealFilament_"+str(n)+".txt", "w")
f.write("%s,\t%s\n" % (tomoFile,segFile))
f.write("n = %d\n" % n)

## Get microtubules
tomogram = getTomo(tomoFile)
segment = getSeg(segFile)
filaments = list(segment.values())
testFil = filaments[n]

## METHOD 1
print("Method 1")
f.write("# # # # # # # # # # METHOD 1: SEARCH # # # # # # # # # #\n")
# create a series of binned masks
f.write("Bin Factor: %d\n" %16)
start_time=time.time()
filTestImage_bin16 = inSegImage(testFil, binFactor=16)
f.write("--- %s seconds ---\n" % (time.time() - start_time))
f.write("Bin Factor: %d\n" %8)
start_time=time.time()
filTestImage_bin8 = inSegImage(testFil, binFactor=8, mask=filTestImage_bin16)
f.write("--- %s seconds ---\n" % (time.time() - start_time))
f.write("Bin Factor: %d\n" %4)
start_time=time.time()
filTestImage_bin4 = inSegImage(testFil, binFactor=4, mask=filTestImage_bin8)
f.write("--- %s seconds ---\n" % (time.time() - start_time))
f.write("Bin Factor: %d\n" %2)
start_time=time.time()
filTestImage_bin2 = inSegImage(testFil, binFactor=2, mask=filTestImage_bin4)
f.write("--- %s seconds ---\n" % (time.time() - start_time))
f.write("Bin Factor: None\n")
start_time=time.time()
filTestImage1 = inSegImage(testFil, mask=filTestImage_bin2)
f.write("--- %s seconds ---\n" % (time.time() - start_time))

## METHOD 2
print("Method 2")
f.write("# # # # # # # # # # METHOD 2: CALCULATION # # # # # # # # # #\n")
start_time=time.time()
filTestImage2 = inSegImage2(testFil)
f.write("--- %s seconds ---\n" % (time.time() - start_time))

## Compare two methods
print("Compare")
f.write("# # # # # # # # # # COMPARING METHODS # # # # # # # # # #\n")
f.write("# voxels in method 1:\t\t\t%d\n" %int(np.sum(filTestImage1)))
f.write("# voxels in method 2:\t\t\t%d\n" % int(np.sum(filTestImage2)))
f.write("difference in method 1 and method 2:\t%d\n" % int(np.sum(filTestImage2!=filTestImage1)))
f.write("# voxels in method 1 not in method 2:\t%d\n" %int(np.sum(filTestImage1*(filTestImage2!=filTestImage1))))
f.write("# voxels in method 2 not in method 1:\t%d\n" %int(np.sum(filTestImage2*(filTestImage2!=filTestImage1))))

f.close()

## Plotting
print("Plotting")
# Method 1
fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection='3d')
ax.axes.set_xlabel("x")
ax.axes.set_ylabel("y")
ax.axes.set_zlabel("z")
ax.voxels(filTestImage1, facecolors='r');
ax.voxels(filTestImage_bin2, facecolors='#1f77b430', shade=False);
ax.set_box_aspect(filTestImage1.shape/np.max(filTestImage1.shape))
plt.savefig('figures/Fil_'+str(n)+'_Method1.png')
# Method 2
fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection='3d')
ax.axes.set_xlabel("x")
ax.axes.set_ylabel("y")
ax.axes.set_zlabel("z")
ax.voxels(filTestImage2, facecolors='b');
ax.set_box_aspect(filTestImage2.shape/np.max(filTestImage2.shape))
plt.savefig('figures/Fil_'+str(n)+'_Method2.png')
# Compare
fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection='3d')
ax.axes.set_xlabel("x")
ax.axes.set_ylabel("y")
ax.axes.set_zlabel("z")
ax.voxels(filTestImage1*(filTestImage2!=filTestImage1), facecolors='#ff000010',edgecolors=None)
ax.voxels(filTestImage2*(filTestImage2!=filTestImage1), facecolors='#0000ff10',edgecolors=None)
ax.set_box_aspect(filTestImage1.shape/np.max(filTestImage1.shape))
plt.savefig('figures/Fil_'+str(n)+'_Compare.png')

