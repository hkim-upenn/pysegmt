import math
import mrcfile
import os
from glob import glob
import multiprocessing as mp
import numpy as np
import h5py
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import scipy
import scipy.interpolate
import time

from scipy.interpolate import RegularGridInterpolator
from Bio.PDB import PDBParser
from PIL import Image
from pathlib import Path
from scipy.spatial.transform import Rotation
from ipywidgets import *
from matplotlib.widgets import Slider
from functools import partial

DEFAULT_DIM = [1024,1024,400] # dimension of tomogram in pixels
DEFAULT_APIX = 2.65 # angstrom/pixel factor
DEFAULT_TOMOGRAMBIN = 4 # binning of the tomograms
DEFAULT_RANG = 100 # radius of interest in angstroms
DEFAULT_MTRANG = 270/2 # radius of microtubule in angstroms
DEFAULT_RPIX = (DEFAULT_RANG+DEFAULT_MTRANG)/(DEFAULT_APIX*DEFAULT_TOMOGRAMBIN) # radius of interest in pixels
DEFAULT_NTHREADS = int(3*mp.cpu_count()/4) # number of threads for parallel processing
results = []

# Get tomogram file
def getTomo(tomoFile):
    f = h5py.File(tomoFile, 'r')
    data = f.get('/MDF/images/0/image')
    return data[:]
        
# Get filaments from segmentation file
def getSeg(segFile):
    pdbParser = PDBParser()  
    structure = pdbParser.get_structure("microtubule", segFile)
    # dict of {chain ID : [xpos, ypos, zpos]}
    for model in structure:
        filaments = {} 
        for filament in model:
            atoms = filament.get_atoms()
            b = len([_ for _ in filament.get_atoms()])
            positions = np.zeros([3,b],dtype=float)
            i=0
            for atom in atoms:
                coord = atom.get_coord()
                for j in range(3):
                    positions[j,i] = coord[j]
                i=i+1
            filaments[filament.get_id()] = positions.T
    return filaments

# View 3D image through 2D projections along z-axis
def sliceShow(image, valinit = 0):
    fig= plt.figure()
    ax=fig.add_subplot(111)
    ax.imshow(image[:,:,valinit],cmap=plt.get_cmap('gray'))
    slider_ax = fig.add_axes([0.2, 0, 0.6, 0.05])
    samp = Slider(slider_ax, 'slice', 0, image.shape[2]-1, valinit=valinit,valstep=1)
    def update(val):
        ax.imshow(image[:,:,samp.val],cmap=plt.get_cmap('gray'))
        fig.canvas.draw_idle()
    samp.on_changed(update);
    
def quickPlot(image, skip=None, save=None, color=None):
    image_coords = np.argwhere(image)
    if skip is not None:
        image_coords = image_coords[0:image_coords.shape[0]:skip]
    
    fig = plt.figure(figsize=(10, 10))
    ax = plt.axes(projection='3d')
    ax.axes.set_xlabel("x")
    ax.axes.set_ylabel("y")
    ax.axes.set_zlabel("z")
    ax.axes.set_xlim3d(left=0, right=image.shape[0]) 
    ax.axes.set_ylim3d(bottom=0, top=image.shape[1]) 
    ax.axes.set_zlim3d(bottom=0, top=image.shape[2])
    if color is not None:
        ax.scatter3D(image_coords[:,0],image_coords[:,1],image_coords[:,2], marker=".", color=color)
    else:
        ax.scatter3D(image_coords[:,0],image_coords[:,1],image_coords[:,2], marker=".")
    ax.set_box_aspect(image.shape/np.max(image.shape))
    
    if save is not None:
        plt.savefig(save+'.png')
        
def coords2Image(coords, dim = DEFAULT_DIM):
    image = np.zeros(dim)
    # remove invalid coordinates
    coords = coords[(coords[:,0]<dim[0]) & (coords[:,1]<dim[1]) & (coords[:,2]<dim[2]) & (coords[:,0]>=0) & (coords[:,1]>=0) & (coords[:,2]>=0)]
    # fill image
    for p in coords:
        image[p[0], p[1], p[2]] = 1
    return image
    
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Calculate orthogonal distance of point to line segment
# p - test point, a - line segment point 1, b - line segment point 2
def lineseg_dist(p, a, b):
    # normalized tangent vector
    d = np.divide(b - a, np.linalg.norm(b - a))
    # signed parallel distance components
    s = np.dot(a - p, d)
    t = np.dot(p - b, d)
    # clamped parallel distance
    h = np.maximum.reduce([s, t, 0])
    # perpendicular distance component
    c = np.cross(p - a, d)
    return np.hypot(h, np.linalg.norm(c))

# Down/Upscale a 3D image
def rescale(coords, dim=DEFAULT_DIM, binFactor=1, scaleDown=True):
    binArray = binFactor*np.ones(3)
    if scaleDown:
        steps = [1,1,1]    # original step sizes
        dx, dy, dz = binArray   # new step sizes
    else:
        steps = binArray    # original step sizes
        dx, dy, dz = [1,1,1]   # new step sizes 
    image = coords2Image(coords, dim=np.divide(dim, steps).astype(int))
    x, y, z = [steps[k] * np.arange(image.shape[k]) for k in range(3)]  # original grid
    f = RegularGridInterpolator((x, y, z), image, method="nearest")    # interpolator
    new_grid = np.mgrid[0:x[-1]:dx, 0:y[-1]:dy, 0:z[-1]:dz]   # new grid
    new_grid = np.moveaxis(new_grid, (0, 1, 2, 3), (3, 0, 1, 2))  # reorder axes for evaluation
    return np.argwhere(f(new_grid))

# Find pixels within radius of line formed by two endpoints, adding spheres at each endpoints
def inFilCoords_search(filament, r=DEFAULT_RPIX, dim=DEFAULT_DIM, binFactor=1, mask=None):
    n = filament.shape[0] # number of points defining the segmentation
    # apply binning if needed
    if binFactor > 1:
        filament = filament/binFactor
        dimCopy = np.divide(dim,binFactor).astype(int)
    else:
        dimCopy = np.copy(dim)
    coords = (filament+0.5).astype(int) 
    idx = 0
    # no mask
    if mask is None:
        for x in range(dimCopy[0]):
            for y in range(dimCopy[1]):
                for z in range(dimCopy[2]):
                    p = np.array([x,y,z])
                    # if the point is not already marked...
                    if not (coords == p).all(axis=1).any():
                        # for each pair of line segments...
                        for i in range(n-1):
                            # is the point near the sphere about the start segment point?
                            if np.linalg.norm(p+0.5 - filament[i]) <= r:
                                coords = np.append(coords,[p],axis=0)
                                break
                            # is the point near the cylinder about the line formed by two segment points?
                            elif lineseg_dist(p+0.5, filament[i], filament[i+1]) <= r:
                                coords = np.append(coords,[p],axis=0)
                                break
                        # is the point in the sphere about the end point
                        if np.linalg.norm(p+0.5 - filament[n-1]) <= r:
                            coords = np.append(coords,[p],axis=0)
    # apply mask
    else:
        # rescale mask if binFactor given
        if binFactor > 1:
            mask = rescale(mask, dim=dim, binFactor=binFactor, scaleDown=True)
        for x, y, z in mask:
            p = np.array([x,y,z])
            # if the point is not already marked...
            if not (coords == p).all(axis=1).any():
                # for each pair of line segments...
                for i in range(n-1):
                    # is the point near the sphere about the start segment point?
                    if np.linalg.norm(p+0.5 - filament[i]) <= r:
                        coords = np.append(coords,[p],axis=0)
                        break
                    # is the point near the cylinder about the line formed by two segment points?
                    elif lineseg_dist(p+0.5, filament[i], filament[i+1]) <= r:
                        coords = np.append(coords,[p],axis=0)
                        break
                # is the point in the sphere about the end point
                if np.linalg.norm(p+0.5 - filament[n-1]) <= r:
                    coords = np.append(coords,[p],axis=0)
    coords = np.unique(coords, axis=0)
    # convert back into original coordinates for binning
    if binFactor > 1:
        coords = rescale(coords, dim=dim, binFactor=binFactor, scaleDown=False)
    return coords

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Creates a rotation matrix that aligns two vectors
def genRotMatrix(vec1, vec2=np.array([0, 0, 1])):
    vec1 = np.reshape(vec1, (1, -1))
    vec2 = np.reshape(vec2, (1, -1))
    r = Rotation.align_vectors(vec1, vec2)
    return r[0].as_matrix()

# Returns a list of integer coordinates within a cylinder
def genCylCoords(start, end, r=DEFAULT_RPIX, axisFactor=2, thetaN=180, rFactor=1.2):
    height = np.linalg.norm(end-start)
    axisN = int(axisFactor*np.ceil(height))+1
    rN = int(rFactor*r)
    axisList = np.append(np.zeros([axisN,2]), np.array([np.linspace(-height/2,height/2,axisN)]).T ,axis=1)
    thetaList = np.linspace(0, 2*math.pi, thetaN+1)
    rList = np.linspace(0, r, rN+1)
    coords = np.zeros([axisList.size,rList.size,thetaList.size,3])
    for i, axis in enumerate(axisList):
        for j, r in enumerate(rList):
            for k, t in enumerate(thetaList):
                coords[i,j,k,:] = axis + [r*math.cos(t),r*math.sin(t),0]
    # list of float coordinates that are within a cylinder whose axis is parallel to the z axis and origin at zero
    coords = np.reshape(coords,[axisList.size*rList.size*thetaList.size,3])
    # rotated cylinder
    coords = np.unique(((genRotMatrix(end-start).dot(coords.T)).T+np.mean([start,end],axis=0)).astype(int),axis=0)
    return coords

# Returns a list of integer coordinates within a sphere
def genSphereCoords(point, r=DEFAULT_RPIX, thetaN=180, rFactor=1.2, phiN=90):
    rN = int(rFactor*r)
    phiList = np.linspace(0, math.pi, phiN+1)
    thetaList = np.linspace(0, 2*math.pi, thetaN+1)
    rList = np.linspace(0, r, rN)
    coords = np.zeros([phiList.size,rList.size,thetaList.size,3])
    for i, p in enumerate(phiList):
        for j, r in enumerate(rList):
            for k, t in enumerate(thetaList):
                coords[i,j,k,:] = point + [r*math.sin(p)*math.cos(t),r*math.sin(p)*math.sin(t),r*math.cos(p)]
    coords = np.reshape(coords,[phiList.size*rList.size*thetaList.size,3])
    coords = np.unique((coords).astype(int),axis=0)
    return coords

# Find pixels within radius of line formed by two endpoints, adding spheres at each endpoints
def inFilCoords_calc(filament, r=DEFAULT_RPIX, axisFactor=2, thetaN=180, phiN=90, rFactor=1.2):
    n = filament.shape[0] # number of points defining the segmentation
    coords = []
    # get first sphere
    coords = genSphereCoords(filament[0],r,thetaN=thetaN,phiN=phiN,rFactor=rFactor)
    # subsequent
    for i in range(n-1):
        # get cylinder surrounding the line segment between points
        coords = np.append(coords,genCylCoords(filament[i],filament[i+1],r=r,axisFactor=axisFactor,thetaN=thetaN,rFactor=rFactor), axis=0)
        # get sphere around end point
        coords = np.append(coords,genSphereCoords(filament[i+1],r=r,thetaN=thetaN,phiN=phiN,rFactor=rFactor), axis=0)
    #  keep only unique values
    coords = np.unique(coords,axis=0).astype(int)
    return coords

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def get_result(result):
    global results
    results.append(result)

# Find pixels that are "near" filaments
# PARALLEL OVER FILAMENTS
# MULTIPLE CPU THREADS, NO GPU
def inFilCoords_calc_ParallelFil(filaments, r=DEFAULT_RPIX, nThreads=DEFAULT_NTHREADS):
    global results
    # init multiprocessing.Pool()
    pool = mp.Pool(nThreads)
    # use `pool.apply` to divide work up per filament
    [pool.apply_async(inFilCoords_calc, args=(filament, r), callback=get_result) for filament in filaments]
    # close pool
    pool.close()
    pool.join() 
    # get coords
    coords = np.unique(np.concatenate(results, axis=0), axis=0)
    results = []
    # image - masked together
    return coords

# Find pixels that are "near" filaments
# PARALLEL OVER SEGMENTS
# MULTIPLE CPU THREADS, NO GPU
def inFilCoords_calc_ParallelSeg(filaments, r=DEFAULT_RPIX, nThreads=DEFAULT_NTHREADS):
    global results
    # init multiprocessing.Pool()
    pool = mp.Pool(nThreads)
    for filament in filaments:
        n = filament.shape[0] # number of points defining the segmentation
        # create pairs of coordinates to analyze
        segmentCoordPair = list(zip(filament[0:-1],filament[1:]))
        # use `pool.apply` to divide work up for CYLINDER PER LINE SEGMENT, and then combine results
        [pool.apply_async(genCylCoords, args=(start, end, r), callback=get_result) for (start, end) in segmentCoordPair]
        # use `pool.apply` to divide work up for SPHERE PER POINT, and then combine results
        [pool.apply_async(genSphereCoords, args=(point, r), callback=get_result) for point in filament]
    # close pool
    pool.close()
    pool.join()
    # combine coordinates and keep only unique values
    coords = np.unique(np.concatenate(results,axis=0),axis=0).astype(int)
    results=[]
    return coords

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def growPixel(p, coords):
    x,y,z = p
    newPix = np.array([[x+1,y,z],[x-1,y,z],[x,y+1,z],[x,y-1,z],[x,y,z+1],[x,y,z-1]],dtype=int)
    coordsToAdd = np.array([])
    for i in newPix:
        if not (i == coords).all(1).any():
            if coordsToAdd.shape[0] > 0:
                coordsToAdd = np.append(coordsToAdd,[i],axis=0)
            else:
                coordsToAdd = np.array([i])
    return coordsToAdd

# Python3 code for generating points on a 3-D line 
# using Bresenham's Algorithm
# https://www.geeksforgeeks.org/bresenhams-algorithm-for-3-d-line-drawing/
def Bresenham3D(p1, p2):
    x1, y1, z1 = p1
    x2, y2, z2 = p2
    coords = np.array([p1])
    dx = abs(x2 - x1)
    dy = abs(y2 - y1)
    dz = abs(z2 - z1)
    if (x2 > x1):
        xs = 1
    else:
        xs = -1
    if (y2 > y1):
        ys = 1
    else:
        ys = -1
    if (z2 > z1):
        zs = 1
    else:
        zs = -1
    # Driving axis is X-axis"
    if (dx >= dy and dx >= dz):        
        p1 = 2 * dy - dx
        p2 = 2 * dz - dx
        while (x1 != x2):
            x1 += xs
            if (p1 >= 0):
                y1 += ys
                p1 -= 2 * dx
            if (p2 >= 0):
                z1 += zs
                p2 -= 2 * dx
            p1 += 2 * dy
            p2 += 2 * dz
            coords = np.append(coords,np.array([[x1,y1,z1]]), axis=0)
    # Driving axis is Y-axis"
    elif (dy >= dx and dy >= dz):       
        p1 = 2 * dx - dy
        p2 = 2 * dz - dy
        while (y1 != y2):
            y1 += ys
            if (p1 >= 0):
                x1 += xs
                p1 -= 2 * dy
            if (p2 >= 0):
                z1 += zs
                p2 -= 2 * dy
            p1 += 2 * dx
            p2 += 2 * dz
            coords = np.append(coords,np.array([[x1,y1,z1]]), axis=0)
    # Driving axis is Z-axis"
    else:        
        p1 = 2 * dy - dz
        p2 = 2 * dx - dz
        while (z1 != z2):
            z1 += zs
            if (p1 >= 0):
                y1 += ys
                p1 -= 2 * dz
            if (p2 >= 0):
                x1 += xs
                p2 -= 2 * dz
            p1 += 2 * dy
            p2 += 2 * dx
            coords = np.append(coords,np.array([[x1,y1,z1]]), axis=0)
    return coords.astype(int)

def growSeg(p1, p2, r=DEFAULT_RPIX):
    # get pixels between segments
    coords = Bresenham3D(p1, p2)
    # grow pixels about the initial set of points
    coordsAdded = np.array([])
    for p in coords:
        coordsToAdd = growPixel(p, coords)
        if coordsToAdd.shape[0] > 0:
            if coordsAdded.shape[0] > 0:
                coordsAdded = np.append(coordsAdded,coordsToAdd,axis=0)
            else:
                coordsAdded = coordsToAdd
    coords=np.append(coords,coordsAdded,axis=0)
    # grow pixels about the set of coordinates r times
    for i in range(r):
        coordsAddedNew = np.array([])
        for p in coordsAdded:
            coordsToAdd = growPixel(p, coords)
            if coordsToAdd.shape[0] > 0:
                if coordsAddedNew.shape[0] > 0:
                    coordsAddedNew = np.append(coordsAddedNew,coordsToAdd,axis=0)
                else:
                    coordsAddedNew = coordsToAdd
        coords = np.unique(np.append(coords,coordsAddedNew,axis=0),axis=0)
        coordsAdded = coordsAddedNew
    return coords

def inFilCoords_grow(filament, r=DEFAULT_RPIX):
    segmentCoordPair = list(zip(filament[0:-1],filament[1:]))
    coords = np.array([]) 
    for p1, p2 in segmentCoordPair:
        if coords.shape[0] > 0:
            coords = np.append(coords, growSeg(p1, p2, r=r),axis=0)
        else:
            coords = growSeg(p1, p2, r=r)
    coords = np.unique(coords,axis=0).astype(int)
    return coords

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def applyMask(tomogram, mask):
    # mask dimensions from (z, x, y) --> (x, y, z) to be consistent with eman2 input
    if np.any(mask.shape != (400,1024,1024)):
        maskTemp = np.moveaxis(mask, -1, 0)
        maskTemp = np.swapaxes(maskTemp, 1,2)
    else:
        maskTemp = mask
    # mask dimensions from (z, x, y) --> (x, y, z) to be consistent with eman2 input
    if np.any(tomogram.shape != (400,1024,1024)):
        tomogramTemp = np.moveaxis(tomogram, -1, 0)
    else:
        tomogramTemp = tomogram
    return np.multiply(maskTemp, tomogramTemp).astype("float32")
    
def saveTomogram(tomogram, tomoFile, suffix=""):
    savename = tomoFile.replace(".hdf","_MTmasked"+suffix+".mrc")
    with mrcfile.new(savename,overwrite=True) as mrc:
        mrc.set_data(tomogram)
