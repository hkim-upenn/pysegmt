clear

% PARAMETERS
% suffix = '2022_06_02';
suffix = 'centralLine_filSeparate';
cropSize = 40;
nFil = 2;

% FILE MANAGEMENT
workDir = '/ChangLab5-hd2/hannah/';
catalDir = strcat(workDir,'t_gondii_dynamo_',suffix);
csvDir=strcat(workDir,'data/T_gondii/');
cd(workDir)
files = dir();
filenames = {files(3:end).name};
groupIdxs = ~cellfun(@isempty,cellfun(@(x) regexp(x,'.*.vll','match'),filenames,'UniformOutput',false));
vllFiles = filenames(groupIdxs);
if ~exist(catalDir, 'dir')
   mkdir(catalDir)
   cellfun(@(x) copyfile(x,catalDir), vllFiles, 'UniformOutput',false);
end
cd(catalDir)
groups=cellfun(@(x) x(1:end-4), vllFiles, 'UniformOutput',false);

% FOR EACH GROUP...
for g=1:length(groups)
    % get group name
    vllFile = vllFiles{g};
    group = vllFile(1:end-4);
    % get tomograms of the group
    tomogramPaths = readlines(strcat(workDir,vllFile));
    nFiles = size(tomogramPaths,1); % number of tomograms
    % create catalogue
    dcm("create",group,"fromvll",vllFile,"delete_old",true)
    % add tomograms to map file (keeps track of index and tomogram name)
    tableMapFile = strcat(group,'_tableMap.doc');
    fileID = fopen(tableMapFile,'w');
    for volNum=1:nFiles
        fprintf(fileID,'%d   %s\n', volNum, tomogramPaths{volNum});
    end
    fclose(fileID);
    % cell to input all tables for merging
    allTables = cell(nFil,nFiles); 
    % for each tomogram
    for volNum=1:nFiles
        % get tomogram name
        [~,tomogramID,~] = fileparts(tomogramPaths{volNum});
        % get information from csv file
%             csvname = strcat(csvDir,group,'/particles/',suffix,'/',tomogramID,'_preproc/particles_',suffix,'.csv');
        csvname = strcat(csvDir,group,'/particles/centralLine/',tomogramID,'_preproc/particles_centralLine.csv');
        rFull = readtable(csvname,'VariableNamingRule','preserve');

        % get points
        tdfname = strcat(csvDir,group,'/segmentations/',tomogramID,'_preproc.txt');
        sFull = table2array(readtable(tdfname,'VariableNamingRule','preserve'));

        % for each filament
        for k=1:nFil
            % label formatting
            fil = int2str(k-1);
            if (k-1) < 10 
                fil = strcat('0',fil);
            end
            % directory to save particles in
            pDir = strcat(group,'_croppedParticles_fil',fil);
            if ~exist(pDir, 'dir')
                mkdir(pDir)
            end
            % select rows matching filament
            r = rFull(rFull.filament==(k-1),:);
            points = sFull(sFull(:,2)==k,3:5);
            
            % create filament model
            m = dmodels.filamentSubunitsInHelix();
            m.radius = 9;
            m.subunits_dphi = 28;
            m.subunits_dz = 20;
            m.points = points;
            m.backboneUpdate();
            m.updateCrop();

%             figure; h = subplot(1,1,1); m.plotTablePoints(h,'refresh',false,'hold_limits',false); drawnow;

%             % generate general model for each point, create crop point
%             m = dmodels.general();
%             % add coordinate info
%             m.addPoint(table2array(r(:,6:8)));
%             % add euler angle info
%             m.crop_angles = table2array([r(:,11),r(:,10),r(:,9)]);
%             m.updateCrop();
%             
            % convert to to table map
            t = m.grepTable();
            % add volume number info
            t(:,20) = volNum;
            % add tomogram information to running list on all tomograms
            allTables{k,volNum} = t;
            % add the model to volume in your project catalogue
            dcm('-c',group,'-i',volNum,'-am',m,'-modelname',strcat(tomogramID,'_fil',fil))
        end
    end

    for k=1:nFil
        % merge all the tables of models in this tomogram and crop particles
        tGlobal = dtmerge(allTables(k,:),'linear_tags',1);
        % label formatting
        fil = int2str(k-1);
        if (k-1) < 10 
            fil = strcat('0',fil);
        end
        % directory to save particles in
        pDir = strcat(group,'_croppedParticles_fil',fil);
        %     % tGlobal = dynamo_table_rescale(tGlobal,'f',4);
        dtcrop(tableMapFile,tGlobal,pDir,cropSize);
        %     % plot particles and axes
%             t1=dread(strcat(pDir,'/crop.tbl')); figure; dtplot(t1(1:end,:),'m','sketch','sketch_length',100','sm',30,'pf','oriented_positions')
    end

    % clear variables for next iteration
    clearvars allTables tGlobal
end

% createAlignedParticles