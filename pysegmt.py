from pysegmt_utils import *
import argparse

'''
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Notes
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Running this pipeline makes several assumptions about the way that the
previous parts of the run have been performed:
    1. The tomograms have gone through the Eman2 pipeline for segment-
       ation. The work directory should ideally have the following dir-
       ectories (*** - indicates ones required for this script)
           - info/
           - neuralnets/
           - particles/
           - segmentations/***
           - tomograms/***
    2. All tomograms to be masked meet the following requirements:
           - Located in tomograms/
           - Filename ends with "_preproc.hdf"
    3. All segmentation files meet the following requirements:
           - Located in segmentations/
           - Contains information on points defining central line, org-
             anized by filaments
           - Has same filename as the corresponding tomogram it is
             segmented from (hence, will also end in "_preproc")
           - For multiple tomograms to mask, each segmentation file has
             the same file extension: either .cmm or .pdb. Which one to
             be used is specified by the variable segType. If following
             the instructions for automated central line generation in
             Chimera, the file type should be .cmm.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
'''

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Arguments
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
parser = argparse.ArgumentParser(description="Masks tomograms given segmentation of filament central lines",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--segType", help="extension type of segmentation files, either \"cmm\" or \"pdb\"", default="cmm")
parser.add_argument("--suffix", help="suffix for output", default="")
parser.add_argument("--rOut", help="outer radius of mask in Angstroms", default=DEFAULT_RANG_OUTER)
parser.add_argument("--rIn", help="inner radius of mask in Angstroms", default=DEFAULT_RANG_INNER)
parser.add_argument("--hollow", help="mask the inner filament, either True or False", default="True")
parser.add_argument("--saveFil", help="save csv data on each microtubule filament's coordinates and intensity", default="True")
parser.add_argument("--unroll", help="unroll the masked tomogram", default="False")
parser.add_argument("--genParticles", help="generate particle subtomograms", default="True")
parser.add_argument("--axisStep", help="step distance in pixels to traverse microtubule", default=23)
parser.add_argument("--rN", help="number of equally spaced distances from central line to sample points from", default=1)
parser.add_argument("--thetaN", help="number of equally spaced angular positions about central line to sample points from", default=13)
parser.add_argument("--stDim", help="box size of subtomgoram in pixels", default=40)

parser.add_argument("--nThreads", help="number of threads", default=DEFAULT_NTHREADS)
args = parser.parse_args()
config = vars(args)
segType = config["segType"].lower()
if (segType != "cmm") and (segType != "pdb") and (segType != "txt"):
    print("Error: segType must be either \"cmm\" or \"pdb\" or \"txt\"")
    sys.exit()
suffix =  config["suffix"]
rOut = float(config["rOut"])/(DEFAULT_APIX*DEFAULT_TOMOGRAMBIN)
rIn = float(config["rIn"])/(DEFAULT_APIX*DEFAULT_TOMOGRAMBIN)
if rOut < rIn:
    print("Warning: rOut is less than rIn. Swapping values...")
    rTemp = rIn
    rIn = rOut
    rOut = rTemp
nThreads =  int(config["nThreads"])
hollow = config["hollow"]
if hollow.lower() not in ['true', '1', 'false', '0']:
    print("Error: maskInner must be type bool, \"True\", \"1\", \"False\",\"0\"")
    sys.exit()
if hollow.lower() in ['true', '1']:
    maskInner = True
else:
    maskInner = False
saveFil = config["hollow"]
if saveFil.lower() not in ['true', '1', 'false', '0']:
    print("Error: saveFil must be type bool, \"True\", \"1\", \"False\",\"0\"")
    sys.exit()
if saveFil.lower() in ['true', '1']:
    saveFil = True
else:
    saveFil = False
unroll = config["hollow"]
if unroll.lower() not in ['true', '1', 'false', '0']:
    print("Error: unroll must be type bool, \"True\", \"1\", \"False\",\"0\"")
    sys.exit()
if unroll.lower() in ['true', '1']:
    unroll = True
else:
    unroll = False
genParticles = config["hollow"]
if genParticles.lower() not in ['true', '1', 'false', '0']:
    print("Error: genParticles must be type bool, \"True\", \"1\", \"False\",\"0\"")
    sys.exit()
if genParticles.lower() in ['true', '1']:
    genParticles = True
else:
    genParticles = False
axisStep = int(config["axisStep"])
rN = int(config["rN"])
thetaN = int(config["thetaN"])
stDim = int(config["stDim"])
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Directory management
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
workDir = os.getcwd()
segDir = os.path.join(workDir,"segmentations")
tomoDir = os.path.join(workDir,"tomograms")
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Get input tomograms and segmentations
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
tomoFiles = list(glob(os.path.join(tomoDir,"*_preproc.hdf")))
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Generate masked tomograms
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
for i in range(len(tomoFiles)):
    segFile = tomoFiles[i].replace(".hdf","."+segType).replace("tomograms/","segmentations/")
    print("TOMOGRAM:\t"+tomoFiles[i])
    print("SEGMENTATION:\t"+segFile)
    if not os.path.exists(segFile):
        print("Could not find segmentation file: " + segFile + "\nSkipping....")
        continue
    processTomogram(tomoFiles[i], segFile, rOut=rOut, rIn=rIn, maskInner=maskInner, nThreads=nThreads, saveFil=saveFil, unroll=unroll, genParticles=genParticles, axisStep=axisStep, rN=rN, thetaN=thetaN, stDim=stDim, suffix=suffix)
    print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 