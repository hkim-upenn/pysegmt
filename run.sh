#!/bin/bash

# parameters
segType="txt"
suffix="centralLine"
rIn=45
rOut=540
axisStep=60
rN=1
thetaN=6
stDim=40

# run in /ChangLab5-hd2/hannah/data/T_gondii
directories=(*)
for i in "${!directories[@]}"; do
    echo ${directories[i]}
    cd ${directories[i]}
    subdirectories=(particles/*/)
    for j in "${!subdirectories[@]}"; do
    particleDir=${subdirectories[j]}particles/${suffix}
    if [ -d "$particleDir" ]; then
        rm -r ${particleDir}
    fi
#         files=(${subdirectories[j]}${suffix}/particle*${suffix}*)
    done
    python ~/pysegmt/pysegmt.py --segType ${segType} --suffix ${suffix} --rIn ${rIn} --rOut ${rOut} --axisStep ${axisStep} --rN ${rN} --thetaN ${thetaN} --stDim ${stDim}
    cd ..
done