from pysegmt import *

workDir="/ChangLab5-hd2/hannah/data/test/"

f = open("TestFilament.txt", "w")

# dim = [50,50,50], 4 points defining 3 line segments
test = np.array([[5,10,30,25],[34,20,34,26],[46,23,14,4]]).T

## Method 1: Full search (w/ and w/out binned masks)
f.write("# # # # # # # # # # METHOD 1: SEARCH # # # # # # # # # #\n")
# no binning
f.write("bin=None, mask=None\n")
start_time = time.time()
testImage1 = inSegImage(test, 3, [50,50,50])
f.write("--- %s seconds ---\n" % (time.time() - start_time))

fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection='3d')
ax.axes.set_xlabel("x")
ax.axes.set_ylabel("y")
ax.axes.set_zlabel("z")
ax.voxels(testImage1, facecolors='r');
plt.savefig('figures/Test_Method1.png')

# binning by factor of 2
f.write("bin=2, mask=None\n")
start_time=time.time()
testImage_bin = inSegImage(test, 3, [50,50,50], binFactor=2)
f.write("--- %s seconds ---\n" % (time.time() - start_time))

fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection='3d')
ax.axes.set_xlabel("x")
ax.axes.set_ylabel("y")
ax.axes.set_zlabel("z")
ax.voxels(testImage_bin, facecolors='r');
plt.savefig('figures/Test_Method1_bin.png')

# no binning but using the mask from data binned by factor of 2
f.write("bin=None, mask=(bin=2)\n")
start_time=time.time()
testImage_maskUsed = inSegImage(test, 3, [50,50,50], mask=testImage_bin)
f.write("--- %s seconds ---\n" % (time.time() - start_time))

fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection='3d')
ax.axes.set_xlabel("x")
ax.axes.set_ylabel("y")
ax.axes.set_zlabel("z")
ax.voxels(testImage_maskUsed, facecolors='r');
ax.voxels(testImage_bin, facecolors='#1f77b430', edgecolors='gray', shade=False);
plt.savefig('figures/Test_Method1_masked.png')

## Method 2: Direct calculation
f.write("# # # # # # # # # # METHOD 2: CALCULATION # # # # # # # # # #\n")
start_time = time.time()
testImage2 = inSegImage2(test, 3, [50,50,50])
f.write("--- %s seconds ---\n" % (time.time() - start_time))

fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection='3d')
ax.axes.set_xlabel("x")
ax.axes.set_ylabel("y")
ax.axes.set_zlabel("z")
ax.voxels(testImage2, facecolors='b');
plt.savefig('figures/Test_Method2.png')

# compare two methods
f.write("# # # # # # # # # # COMPARING METHODS # # # # # # # # # #\n")
f.write("# voxels in method 1:\t\t\t%d\n" % int(np.sum(testImage1)))
f.write("# voxels in method 2:\t\t\t%d\n" % int(np.sum(testImage2)))
f.write("difference in method 1 and method 2:\t%d\n" % int(np.sum(testImage2!=testImage1)))
f.write("# voxels in method 1 not in method 2:\t%d\n" % int(np.sum(testImage1*(testImage2!=testImage1))))
f.write("# voxels in method 2 not in method 1:\t%d\n" % int(np.sum(testImage2*(testImage2!=testImage1))))

fig = plt.figure(figsize=(10, 10))
ax = plt.axes(projection='3d')
ax.axes.set_xlabel("x")
ax.axes.set_ylabel("y")
ax.axes.set_zlabel("z")
ax.voxels(testImage1*(testImage2!=testImage1), facecolors='#ff000010',edgecolors=None)
ax.voxels(testImage2*(testImage2!=testImage1), facecolors='#0000ff10',edgecolors=None);
plt.savefig('figures/Test_Compare.png')

# test to see combinations of hyperparameters that ensure method 2 is just as inclusive as method 1
f.write("# # # # # # # # # # HYPERPARAMETER GRID SEARCH # # # # # # # # # #\n")
aN=3
tN=3
rN=2
pN=4
times = np.zeros(aN*tN*rN*pN)
voxel1not2 = np.zeros(aN*tN*rN*pN)
voxel2not1 = np.zeros(aN*tN*rN*pN)
c = 0
f.write("AxisF.\tTheta\tPhi\tRFactor\t: Time (s)\tVox (1 not 2)\tVox (2 not 1)")
for a in [0.8,1,2]:
    for t in [180,240,360]:
        for p in [90,180]:
            for r in [1,1.2,1.5,2]:
                start_time = time.time()
                i = inSegImage2(test, 3, [50,50,50], axisFactor=a,thetaN=int(t),rFactor=r)
                times[c] = time.time() - start_time
                voxel1not2[c] = int(np.sum(testImage1*(i!=testImage1)))
                voxel2not1[c] = int(np.sum(i*(i!=testImage1)))
                f.write("%2.1f\t%3d\t%3d\t%4.3f\t: %4.3f\t\t%3d\t\t%3d\n" % (a,t,p,r,times[c],voxel1not2[c],voxel2not1[c]))
                c = c + 1
f.close()