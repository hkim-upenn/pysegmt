## Multiple filaments
# Comparing Method 1 to Method 2 on a single, real microtubule filament
from pysegmt import *

workDir="/ChangLab5-hd2/hannah/data/test/"
tomoFiles = list(glob(os.path.join(workDir,"tomograms","TS16_unblur_orig__bin4.hdf")))
segFiles = list(glob(os.path.join(workDir,"segmentations","*.pdb")))
tomoFile = tomoFiles[0]
segFile = segFiles[0]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
## Get microtubules
tomogram = getTomo(tomoFile)
segment = getSeg(segFile)
filaments = list(segment.values())

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
f = open("AllFilaments.txt", "w")
f.write("%s,\t%s\n" % (tomoFile,segFile))

## Parallel over segments
print("Parallel over segments")
f.write("# # # # # # # # # # PARALLEL SEGMENTS, METHOD 2 # # # # # # # # # #\n")
f.write("R_OUT: %5.2f\nR_IN:  %5.2f\n" % (DEFAULT_RPIX_OUTER, DEFAULT_RPIX_INNER))
start_time=time.time()
image=coords2Image(inFilCoords(filaments))
f.write("--- %s seconds ---\n" % (time.time() - start_time))
print("--- %s seconds ---\n" % (time.time() - start_time))

maskedTomogram = applyMask(tomogram, image)
saveTomogram(maskedTomogram, tomoFile, suffix="_2022_05_06")

quickPlot(image,skip=1, save="figures/Fil_All_2022_05_06")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
f.close()
