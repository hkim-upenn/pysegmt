import math
import mrcfile
import os

import multiprocessing as mp
import numpy as np

data_directory = "./data"
output_directory = "./output"

smoothing = 5
weighting = 0.95
iters_smoothing_initial = 3
iters_smoothing_final = 1
iters_weighting = 0
do_booleanify = False
booleanify_threshold = 0.95
black_fg = False
n_threads = 8


def unpacking_apply_along_axis(all_args):
    (func1d, axis, arr, args, kwargs) = all_args
    return np.apply_along_axis(func1d, axis, arr, *args, **kwargs)


def parallel_apply_along_axis(func1d, axis, arr, *args, **kwargs):
    """
    Like numpy.apply_along_axis(), but takes advantage of multiple
    cores.
    """
    # Effective axis where apply_along_axis() will be applied by each
    # worker (any non-zero axis number would work, so as to allow the use
    # of `np.array_split()`, which is only done on axis 0):
    effective_axis = 1 if axis == 0 else axis
    if effective_axis != axis:
        arr = arr.swapaxes(axis, effective_axis)

    # Chunks for the mapping (only a few chunks):
    chunks = [(func1d, effective_axis, sub_arr, args, kwargs)
              for sub_arr in np.array_split(arr, mp.cpu_count())]

    pool = mp.Pool(n_threads)
    individual_results = pool.map(unpacking_apply_along_axis, chunks)
    # Freeing the workers:
    pool.close()
    pool.join()

    return np.concatenate(individual_results)


def smooth_axis(arr):
    return 0.25*arr[:-2]+0.5*arr[1:-1]+0.25*arr[2:]


def smooth(arr):
    arr = parallel_apply_along_axis(smooth_axis, 1, arr)
    arr = parallel_apply_along_axis(smooth_axis, 2, arr)
    return arr


def gen_gradient_axis(arr):
    res = np.zeros((arr.shape[0] - smoothing * 2,), dtype=arr.dtype)
    to_divide_by = 0
    for i in range(smoothing):
        res = res - (arr[i:(len(arr) - smoothing * 2 + i)] * math.pow(weighting, i))
        res = res + (arr[(smoothing+i):(len(arr) - smoothing + i)] * math.pow(weighting, i))

        to_divide_by += math.pow(weighting, i)
    res = res * res / to_divide_by
    return res


def gen_gradient(data):
    along_x = (parallel_apply_along_axis(gen_gradient_axis, 1, data))[:, :, smoothing:-smoothing]
    along_y = (parallel_apply_along_axis(gen_gradient_axis, 2, data))[:, smoothing:-smoothing, :]
    return np.sqrt(along_x * along_x + along_y * along_y)


def weight_by_neighbors_axis(arr):
    res = arr[smoothing:-smoothing]
    to_multiply = np.zeros(res.shape, dtype=arr.dtype)
    to_divide_by = 0
    for i in range(smoothing):
        to_multiply += arr[i:-2 * smoothing + i]
        to_multiply += arr[2 * smoothing - i:len(arr)-i]
        to_divide_by += 2
    res = np.sqrt(res * to_multiply / to_divide_by)
    return res


def weight_by_neighbors(data):
    res = parallel_apply_along_axis(weight_by_neighbors_axis, 2, data)
    res = parallel_apply_along_axis(weight_by_neighbors_axis, 1, res)
    return res


def booleanify_axis(data):
    max_ = 150
    min_ = 0
    threshold = sorted([abs(x) for x in data])[int(len(data) * booleanify_threshold)]
    return [max_ if abs(x) > threshold else min_ for x in data]



def booleanify(data):
    return parallel_apply_along_axis(booleanify_axis, 1, data)


def run():
    total = len(os.listdir(data_directory))
    done = 0
    for filename in os.listdir(data_directory):
        done += 1

        # LOAD MRC FILE
        print(f"Loading {filename}... ({ done }/{ total })")
        with mrcfile.open(os.path.join(data_directory, filename)) as data_file:
            data = data_file.data

        # SMOOTH DATA TO REMOVE NOISE
        for i in range(iters_smoothing_initial):
            print(f"Smoothing... { i+1 }/{ iters_smoothing_initial }")
            data = smooth(data)

        # GENERATE DIFFERENTIALS
        print(f"Processing...")
        result = gen_gradient(data)

        # SMOOTH DATA TO REMOVE NOISE
        for i in range(iters_smoothing_final):
            print(f"Smoothing... { i+1 }/{ iters_smoothing_final }")
            result = smooth(result)

        # WEIGHT EACH DATA POINT BY MAGNITUDE OF NEIGHBORS
        for i in range(iters_weighting):
            print(f"Weighting... { i+1 }/{ iters_weighting }")
            result = weight_by_neighbors(result)

        # "BOOLEANIFY"
        if do_booleanify:
            print(f"Booleanifying...")
            result = booleanify(result)

        # COLORIZE
        if black_fg:
            print(f"Coloring...")
            result = -1 * result

        # SAVE
        print(f"Saving...")
        new_file_name = f"{''.join(os.path.splitext(filename)[:-1])}-gradient{os.path.splitext(filename)[-1]}"
        with mrcfile.new(os.path.join(output_directory, new_file_name), overwrite=True) as output_file:
            output_file.set_data(result.astype('int16'))

        print(f"Done!")
    # BEEP
    print('\007')


if __name__ == '__main__':
    run()
