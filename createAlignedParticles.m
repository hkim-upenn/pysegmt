%{
Hannah Kim
06/07/2022
Yi-Wei Chang Lab

THIS VERSION IS COMPATIBLE WITH PEET 1.15.0. IF USING ANOTHER VERSION,
CHECK FOR DIFFERENT SCRIPT.

This script will create aligned particles to view in IMOD, similar to what
PEET does with createAlignedParticles.
%}

% run = char(input('Name of alignment project: ','s'));
% runResults = strcat(run,'/results');
% dirInfo = dir(runResults); %Takes note of what's in the results directory of your alignment project
% tf = ismember({dirInfo.name},{'.','..'}); %looks at file and directory names from input pathway. If '.' or '..', puts into variable tf
% dirInfo(tf) = []; %sets rows from tf to empty, basically erasing them
% numdir = length(dirInfo);
% iteLast = num2str(numdir-1);
% particleDir = input('Enter the directory containing your particles: ','s');
particleDirs = {'ICMAP1_croppedParticles_fil00','ICMAP1_croppedParticles_fil01'};%,...
%     'ICMAP2_croppedParticles_fil00','ICMAP2_croppedParticles_fil01',...
%     'WT_croppedParticles_fil00','WT_croppedParticles_fil01'};

for j=1:length(particleDirs)
    particleDir = particleDirs{j};
    newDir = strcat(particleDir,'_alignedParticles/');
    mkdir(newDir);
    path2rt = strcat(particleDir,'/crop.tbl');
    
    rt = dread(path2rt);
    %rt = dtrescale(rt,'f',0.25);
    
    cd(particleDir)
    command = ['ls particle_*.em | awk -F "." ''{print "cp "$1".em ../' newDir '/"$1".mrc"}'' | sh '];
    status = system(command);
    cd('../')
    
    %This block makes a table containing 3 columns, each
    %pertaining to the Euler angles Z(1), X(2), Z(3). Z(1) and Z(3) are swapped for now between the dynamo table and angle file. It will then save to an
    %angles.csv file, be input into MOTL2Slicer, and saved as a new angles csv
    %file in XYZ format and read back into the workspace cell slicerTable
    
    EulerAngles = zeros(size(rt,1),3);
    for i = 1:size(rt,1)
            EulerAngles(i,1) = -rt(i,9);
            EulerAngles(i,2) = -rt(i,8);
            EulerAngles(i,3) = -rt(i,7);
    end
    
    writematrix(EulerAngles,'refinedEulerAngles.csv');
    !MOTL2Slicer refinedEulerAngles.csv refinedSlicerAngles.csv
    slicerTable = csvread('refinedSlicerAngles.csv');
    
    %Create a table with 6 columns: X,Y,Z,Xangle,Yangle,Zangle
    csvSummary = zeros(size(rt,1),6);
    for i = 1:size(rt,1)
        csvSummary(i,1) = rt(i,24)+rt(i,4);
        csvSummary(i,2) = rt(i,25)+rt(i,5);
        csvSummary(i,3) = rt(i,26)+rt(i,6);
            
        csvSummary(i,4) = slicerTable(i,1);
        csvSummary(i,5) = slicerTable(i,2);
        csvSummary(i,6) = slicerTable(i,3);
    end
    
    cd(newDir);
    
    %Apply rotatevol to each particle
    for i = 1:size(rt,1)
        if i < 10 
            num = strcat('00000',string(rt(i,1)));
        elseif i >= 10 && i < 100
            num = strcat('0000',string(rt(i,1)));
        elseif i >= 100 && i < 1000
            num = strcat('000',string(rt(i,1)));
        elseif i >= 1000 && i < 10000
            num = strcat('00',string(rt(i,1)));
        end
        
        command = strcat('rotatevol -a',{' '},string(csvSummary(i,6)),',',string(csvSummary(i,5)),',',string(csvSummary(i,4)),{' '},'particle_',num,'.mrc particle_',num,'_aligned.mrc');
        system(command);
    
        system(strcat('rm particle_',num,'.mrc'));
        
    end
    
    system(strcat('rm *.mrc~'));

    cd('../')
    system('rm refinedEulerAngles.csv refinedSlicerAngles.csv');
end