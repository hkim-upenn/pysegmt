## Multiple filaments
# Comparing Method 1 to Method 2 on a single, real microtubule filament
from pysegmt import *

workDir="/ChangLab5-hd2/hannah/data/test/"
tomoFiles = list(glob(os.path.join(workDir,"tomograms","TS16_unblur_orig__bin4.hdf")))
segFiles = list(glob(os.path.join(workDir,"segmentations","*.pdb")))
tomoFile = tomoFiles[0]
segFile = segFiles[0]

f = open("AllFilaments.txt", "w")
f.write("%s,\t%s\n" % (tomoFile,segFile))

## Get microtubules
tomogram = getTomo(tomoFile)
segment = getSeg(segFile)
filaments = list(segment.values())


################################################################################################### 
###################################################################################################
print("METHOD 2")
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
## Not parallel
print("Not Parallel")
f.write("# # # # # # # # # # NOT PARALLEL, METHOD 2 # # # # # # # # # #\n")
start_time=time.time()
multFilImage_noParallel_coords = inFilCoords_calc(filaments[0])
for i, filament in enumerate(filaments[1:]):
    multFilImage_noParallel_coords=np.append(multFilImage_noParallel_coords,inFilCoords_calc(filament),axis=0)
multFilImage_noParallel=coords2Image(np.unique(multFilImage_noParallel_coords,axis=0))
f.write("--- %s seconds ---\n" % (time.time() - start_time))

quickPlot(multFilImage_noParallel,skip=1, save="figures/Fil_All_Method2_notParallel_r22")

maskedTomogram = applyMask(tomogram, multFilImage_noParallel)
saveTomogram(maskedTomogram, tomoFile, suffix="_2022_05_04")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
## Parallel over filaments
print("Parallel over filaments")
f.write("# # # # # # # # # # PARALLEL FILAMENTS, METHOD 2 # # # # # # # # # #\n")
start_time=time.time()
multFilImage_filPar = coords2Image(inFilCoords_calc_ParallelFil(filaments))
f.write("--- %s seconds ---\n" % (time.time() - start_time))

quickPlot(multFilImage_filPar,skip=1, save="figures/Fil_All_Method2_ParallelFil_r22")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
## Parallel over segments
print("Parallel over segments")
# f.write("# # # # # # # # # # PARALLEL SEGMENTS, METHOD 2 # # # # # # # # # #\n")
start_time=time.time()
multFilImage_segPar=coords2Image(inFilCoords_calc_ParallelSeg(filaments))
# f.write("--- %s seconds ---\n" % (time.time() - start_time))
print("--- %s seconds ---\n" % (time.time() - start_time))

quickPlot(multFilImage_segPar,skip=1, save="figures/Fil_All_Method2_ParallelSeg_r22")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# f.close()

# ## Plotting
# print("Plotting")
# # Parallel
# fig = plt.figure(figsize=(10, 10))
# ax = plt.axes(projection='3d')
# ax.axes.set_xlabel("x")
# ax.axes.set_ylabel("y")
# ax.axes.set_zlabel("z")
# ax.voxels(multFilImage, facecolors='b');
# ax.set_box_aspect(multFilImage.shape/np.max(multFilImage.shape))
# plt.savefig('figures/Fil_All_Method2.png')
# # Not parallel
# fig = plt.figure(figsize=(10, 10))
# ax = plt.axes(projection='3d')
# ax.axes.set_xlabel("x")
# ax.axes.set_ylabel("y")
# ax.axes.set_zlabel("z")
# ax.voxels(multFilImage_noParallel, facecolors='b');
# ax.set_box_aspect(multFilImage_noParallel.shape/np.max(multFilImage_noParallel.shape))
# plt.savefig('figures/Fil_All_Method2_notParallel.png')
