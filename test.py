import math
import mrcfile
import os
from glob import glob
import multiprocessing as mp
import numpy as np
import h5py
import pandas as pd

from Bio.PDB import PDBParser

from pathlib import Path


# REMOVE LATER
workDir="/ChangLab5-hd2/hannah/data/test/"

# Parameters
apix = 2.56 # apix value
binFactor = 4 # what to bin tomogram by
maxR = 10 # max radius in pix to search about MT

def run(workDir):
    # Tomogram reconstruction files
    tomoFiles = list(glob(os.path.join(workDir,"tomograms","TS16_unblur_orig__bin4.hdf")))
    # Segmentation file
    segFiles = list(glob(os.path.join(workDir,"segmentations","*.pdb")))
#     print(tomoFiles, segFiles)
    
    # For each tomogram
    for i in range(len(tomoFiles)):
        # Mask unimportant areas
        mask(tomoFiles[i], segFiles[i])

        # Pick particles

        # Find normal vectors of particle wrt microtubule
    
    # Store collection of particles from all tomograms
    
    # Unsupervised classification of particles
    
    # Feed output to Eman2 analysis

def getTomo(tomoFile, binFactor):
    hf = h5py.File(tomoFile)
    data = hf.get('/MDF/images/0/image')
    return data[:]
        
def getSeg(segFile,binFactor):
    pdbParser = PDBParser()  
    structure = pdbParser.get_structure("microtubule", segFile)
    # dict of {chain ID : [xpos, ypos, zpos]}
    for model in structure:
        filaments = {} 
        for filament in model:
            atoms = filament.get_atoms()
            b = len([_ for _ in filament.get_atoms()])
            positions = np.zeros([3,b],dtype=float)
            i=0
            for atom in atoms:
                coord = atom.get_coord()
                for j in range(3):
                    positions[j,i] = coord[j]
                i=i+1
            filaments[filament.get_id()] = positions
    return filaments

def mask():
    print("TODO")

if __name__ == '__main__':
    run(workDir)
