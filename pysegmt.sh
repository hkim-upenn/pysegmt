#!/bin/bash

shopt -s nullglob

# Parameters
tiltStep="3.0"
shrink=1
boxTomogramIdx=0

# activate eman2 environment
eval "$(conda shell.bash hook)"
conda activate /home/hannahkim/eman2-sphire-sparx/

# conversion of .rec to .mrc if necessary
recFiles=(*.rec)
mrcFiles=(*.mrc)
if [[ ! "${#recFiles[@]}" -eq 0 ]]; then
    if [[ "${#mrcFiles[@]}" -eq 0 ]]; then
        for i in "${!recFiles[@]}"; do 
            mv "${recFiles[$i]}" "${recFiles[$i]/.rec/.mrc}"
        done
    fi
elif [ -d "tomograms" ]; then
    recFiles=(tomograms/*.rec)
    mrcFiles=(tomograms/*.mrc)
    if [[ ! "${#recFiles[@]}" -eq 0 ]]; then
        if [[ "${#mrcFiles[@]}" -eq 0 ]]; then
            for i in "${!recFiles[@]}"; do 
                mv "${recFiles[$i]}" "${recFiles[$i]/.rec/.mrc}"
            done
        fi
    fi
fi

# file management of mrc files
if [ ! -d "tomograms" ]; then
    mkdir tomograms
    mv *.mrc tomograms/
else
    mrcFiles=(tomograms/*.mrc)
    if [[ "${#mrcFiles[@]}" -eq 0 ]]; then
        mv *.mrc tomograms/
    fi
fi
mrcFiles=(tomograms/*.mrc)
delete=(tomograms/*mask*.mrc)
for del in ${delete[@]}
do
   mrcFiles=("${mrcFiles[@]/$del}")
done

echo "******************************************************************************"
echo "******************************************************************************"
echo "1. IMPORTING TOMOGRAMS"
echo "******************************************************************************"
echo "******************************************************************************"
e2import.py ${mrcFiles[@]} --import_tomos --importation=copy --shrink=${shrink}

echo "******************************************************************************"
echo "******************************************************************************"
echo "2. PREPROCESS TOMOGRAMS"
echo "******************************************************************************"
echo "******************************************************************************"
allHdfFiles=(tomograms/*.hdf)
fileN=0
for i in "${!allHdfFiles[@]}"; do 
    if [[ ! ("${allHdfFiles[$i]}" == *preproc*) ]]; then
        hdfFiles[$fileN]="${allHdfFiles[$i]}"
        let fileN+=1
    fi
done
e2tomoseg_preproc.py ${hdfFiles[@]} --proctag=preproc --proc1=filter.lowpass.gauss:cutoff_abs=.25 --proc2=filter.highpass.gauss:cutoff_pixels=5 --proc3=normalize --proc4=threshold.clampminmax.nsigma:nsigma=3 
hdfPreprocFiles=(tomograms/*_preproc.hdf)

echo "******************************************************************************"
echo "******************************************************************************"
echo "3. SET TRAINING EXAMPLES"
echo "******************************************************************************"
echo "******************************************************************************"
echo "   Creates boxes from first tomogram in directory"
echo "   Do not include any ambiguous samples!"
echo "   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
echo "   Have at least one of each of the following classes:"
echo "       good_* - Boxes containing particle of interest. Have at least 10 - 20"
echo "       bad_*  - Boxes without particle of interest. Have at least 100"
echo "   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
echo "   If particle looks very different in different orientations, train multiple,"
echo "   distinct \"good\" classes for each orientation"
echo "   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
e2spt_boxer.py ${hdfPreprocFiles[${boxTomogramIdx}]} --box2d

echo "******************************************************************************"
echo "******************************************************************************"
echo "4. MANUALLY ANNOTATE"
echo "******************************************************************************"
echo "******************************************************************************"
goodParticles=(particles/*good*.hdf)
badParticles=(particles/*bad*.hdf)
for i in "${!goodParticles[@]}"; do 
    e2tomoseg_buildtrainset.py --segment "${goodParticles[$i]}"
done

echo "******************************************************************************"
echo "******************************************************************************"
echo "5. BUILDING TRAINING SET"
echo "******************************************************************************"
echo "******************************************************************************"
# find first good set of particles
firstGood=0
for i in "${!goodParticles[@]}"; do 
    if [[ ! "${goodParticles[$i]}" == *_seg.hdf ]]; then
      firstGood=$i
      break
    fi
done
# create list of good particles
particles_raw=${goodParticles[$firstGood]}
for i in "${!goodParticles[@]}"; do 
    if [[ "$i" == "$firstGood" ]]; then
        continue
    fi
    if [[ "${goodParticles[$i]}" == *_seg.hdf ]]; then
      continue
    fi
    particles_raw="${particles_raw},${goodParticles[$i]}"
done
# create list of segmented good particles
particles_label="${goodParticles[0]/.hdf/_seg.hdf}"
for i in "${!goodParticles[@]}"; do 
    if [[ "$i" == "$firstGood" ]]; then
        continue
    fi
    if [[ "${goodParticles[$i]}" == *_seg.hdf ]]; then
      continue
    fi
    particles_label="${particles_label},${goodParticles[$i]/.hdf/_seg.hdf}"
done
# create list of bad particles
boxes_negative=${badParticles[0]}
for i in "${!badParticles[@]}"; do 
    if [[ "$i" == '0' ]]; then
        continue
    fi
    boxes_negative="${boxes_negative},${badParticles[$i]}"
done
e2tomoseg_buildtrainset.py --buildset --particles_raw="${particles_raw}" --particles_label=${particles_label} --boxes_negative=${boxes_negative} --ncopy=10 --validset=0.0

echo "******************************************************************************"
echo "******************************************************************************"
echo "6. TRAIN NEURAL NETWORK"
echo "******************************************************************************"
echo "******************************************************************************"
for i in "${!particles_raw[@]}"; do 
    e2tomoseg_convnet.py --trainset="${goodParticles[$i]/.hdf/_trainset.hdf}" --learnrate=0.0001 --niter=20 --ncopy=1 --batch=20 --nkernel=40,40,1 --ksize=15,15,15 --poolsz=2,1,1 --trainout --training --device=gpu
done 


echo "******************************************************************************"
echo "******************************************************************************"
echo "7. APPLY TO TOMOGRAMS"
echo "******************************************************************************"
echo "******************************************************************************"
nnet=(neuralnets/nnet*.hdf)
tomograms=${hdfPreprocFiles[0]}
for i in "${!hdfPreprocFiles[@]}"; do 
    if [[ "$i" == '0' ]]; then
        continue
    fi
    tomograms="${tomograms},${hdfPreprocFiles[$i]}"
done

for i in "${!nnet[@]}"; do
    e2tomoseg_convnet.py --nnet=${nnet[$i]} --tomograms=${tomograms} --applying --outtag=_0 --threads=12 --device=gpu
done

# resume original environment
# eval "$(conda shell.bash hook)"
conda activate base
