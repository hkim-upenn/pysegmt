#!/bin/bash

# converts imod model to text file
shopt -s nullglob

files=(segmentations/*)
for i in "${!files[@]}"; do 
    model2point -ob "${file[$i]}" "${files[$i]}.txt"
done