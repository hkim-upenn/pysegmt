#!/bin/bash

shopt -s nullglob

# TODO: Add "fromScratch" option where it either runs function from scratch or checks if each step has been completed
#       if fromScratch==True && (condition checking that function was run), then...

# Parameters
workDir="/ChangLab5-hd2/hannah/data/test/"
tiltStep="3.0"
currentDir=$PWD

# File management
cd $workDir
mrcFiles=(*.mrc)
mdocFiles=(*.mdoc)
for i in "${!mrcFiles[@]}"; do 
    hdfFiles[$i]="tiltseries/${mrcFiles[$i]/.mrc/".hdf"}"
    hdfBinFiles[$i]="tomograms/${mrcFiles[$i]/.mrc/"__bin4.hdf"}"
done
echo "${mrcFiles[@]}"

# activate eman2 environment
eval "$(conda shell.bash hook)"
conda activate /home/hannahkim/eman2-sphire-sparx/

echo "******************************************************************************"
echo "******************************************************************************"
echo "IMPORTING TILTSERIES"
echo "******************************************************************************"
echo "******************************************************************************"
e2import.py ${mrcFiles[@]} --apix=-1.0 --import_tiltseries --compressbits=8 --importation=copy 

echo "******************************************************************************"
echo "******************************************************************************"
echo "RECONSTRUCTING TOMOGRAMS"
echo "******************************************************************************"
echo "******************************************************************************"
e2tomogram.py ${hdfFiles[@]} --alltiltseries --zeroid=-1 --tltstep=3.0 --npk=20 --tltax=-87.0 --tltkeep=0.9 --outsize=1k --niter=2,1,1,1 --bytile --notmp --pkkeep=0.9 --compressbits=8 --clipz=-1 --bxsz=32 --correctrot --filterres=40.0 --rmbeadthr=-1.0 --threads=12 --patchtrack=-1 --posz --xdrift --autoclipxy

echo "******************************************************************************"
echo "******************************************************************************"
echo "CTF ESTIMATION"
echo "******************************************************************************"
echo "******************************************************************************"
e2spt_tomoctf.py ${hdfFiles[@]} --alltiltseries --dfrange=2.0,7.0,0.02 --psrange=10,15,5 --tilesize=256 --voltage=300 --cs=2.7 --nref=15 --stepx=20 --stepy=40 --checkhand --threads=1 

echo "******************************************************************************"
echo "******************************************************************************"
echo "SEGMENTATION"
echo "******************************************************************************"
echo "******************************************************************************"
# Draw curves - saves PDB structure file once all curves are traced, can open in pymol or chimera
# Options:
#       1. Manually trace MT in each tomogram
#       2. Use built-in CNN training mechanism for MT search
#       3. Use template matching to auto-search for MT

echo "******************************************************************************"
echo "******************************************************************************"
echo "PARTICLE PICKING"
echo "******************************************************************************"
echo "******************************************************************************"
# TODO: Insert custom code here that:
#       1. Tracks microtubules
#       2. Gets particles surrounding microtubules
#       3. Sorts selected particles into classes
#          * Label must be different for each different type of particle
#       4. Set check that there are at least 30-50 particles per class

echo "******************************************************************************"
echo "******************************************************************************"
echo "PARTICLE EXTRACTION"
echo "******************************************************************************"
echo "******************************************************************************"
# TODO: For each particle type (specified by "label"):
#            get the orig-res tilt-series per particle in the class
# curves_overlap = 0.8, shrink = 2, boxsz_unbin = 40?

echo "******************************************************************************"
echo "******************************************************************************"
echo "INITIAL MODEL GENERATION"
echo "******************************************************************************"
echo "******************************************************************************"
# For each particle type
# * particles should be set to the sets/<LABEL>.lst file 

echo "******************************************************************************"
echo "******************************************************************************"
echo "SUBTOMOGRAM REFINEMENT"
echo "******************************************************************************"
echo "******************************************************************************"
# Is it necessary? Probably not for now

# resume original environment
# eval "$(conda shell.bash hook)"
conda activate /home/hannahkim/miniconda/