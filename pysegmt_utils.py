import sys
import math
import os
import numpy as np
import pandas as pd
import h5py
import mrcfile
import xml.etree.ElementTree as ET
import multiprocessing as mp
import time
import matplotlib.pyplot as plt
import warnings
from glob import glob
from functools import partial
from Bio.PDB import PDBParser
from scipy.spatial.transform import Rotation
from scipy.interpolate import griddata

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

warnings.filterwarnings('ignore', '.*Optimal rotation is not uniquely or poorly defined for the given sets of vectors.*', )

DEFAULT_DIM = [400, 1024, 1024] # dimension of tomogram in pixels
DEFAULT_APIX = 2.65 # angstrom/pixel factor
DEFAULT_TOMOGRAMBIN = 4 # binning of the tomograms
DEFAULT_RANG_OUTER = 235 # outer radius of interest in angstroms
DEFAULT_RANG_INNER = 60 # outer radius of interest in angstroms
DEFAULT_RPIX_OUTER = (DEFAULT_RANG_OUTER)/(DEFAULT_APIX*DEFAULT_TOMOGRAMBIN) # radius of interest in pixels
DEFAULT_RPIX_INNER = (DEFAULT_RANG_INNER)/(DEFAULT_APIX*DEFAULT_TOMOGRAMBIN) # radius of interest in pixels
DEFAULT_NTHREADS = int(3*mp.cpu_count()/4) # number of threads for parallel processing
DEFAULT_RTHRESH_ANGLEFACTOR = [[0,25,1],[26,35,1.6],[36,45,1.8],[46,55,2],[56,100,2.1]] # [[r lower, r upper, angle factor],...]

results = []

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Get tomogram file
def getTomo(tomoFile):
    ext = os.path.splitext(tomoFile)[1]
    if ext==".hdf":
        f = h5py.File(tomoFile, 'r')
        data = (f.get('/MDF/images/0/image'))[:]
    elif ext==".mrc":
        with mrcfile.open(tomoFile) as mrc:
            data = mrc.data
    return data
        
# Get segmentation points from PDB file produced through manual segmentation (EMAN2)
def PDB2Fil(segFile):
    pdbParser = PDBParser()  
    structure = pdbParser.get_structure("microtubule", segFile)
    # dict of {chain ID : [xpos, ypos, zpos]}
    for model in structure:
        filaments = {} 
        for filament in model:
            atoms = filament.get_atoms()
            b = len([_ for _ in filament.get_atoms()])
            positions = np.zeros([3,b],dtype=float)
            i=0
            for atom in atoms:
                coord = atom.get_coord()
                for j in range(3):
                    positions[j,i] = coord[j]
                i=i+1
            filaments[filament.get_id()] = positions.T
    return list(filaments.values())

# Get segmentation points from CMM file produced through automated segmentation via Segger (Chimera)
def CMM2Fil(cmmFile, minN = 3):
    data = []
    tree = ET.parse(cmmFile)
    root = tree.getroot()
    for filament in root:
        points = np.concatenate([np.array([[float(segment.attrib['x']),float(segment.attrib['y']),float(segment.attrib['z'])]]) for segment in filament if "id" in segment.attrib.keys()], axis=0)
        if points.shape[0] > minN:
            if len(data) < 1:
                data = [points]
            else:
                data.append(points)
    return data

# Get segmentation points from TXT file produced through manual segmentation (IMOD)
def TXT2Fil(segFile):
    with open(segFile) as f:                                                                                          
        df = pd.read_csv(f, delimiter=r"\s+", names=["object","filament","x","y","z"])
        data=[]
        for i in df['filament'].unique():
            df_fil = df[df['filament']==i]
            data = data + [df_fil[['x','y','z']].to_numpy()]
    return data

# Removes 3D coordinates appearing in b_full from a_full
def removeBfromA(a_full,b_full):
    A=a_full[:,0:3]
    B=b_full[:,0:3]
    cumdims = (np.maximum(A.max(),B.max())+1)**np.arange(B.shape[1])
    return a_full[~np.in1d(A.dot(cumdims),B.dot(cumdims))]

# Used for parallel processing
def collect_result(result):
    global results
    results.append(result)
    
# Formats suffix to prevent double underscores in filenames
def formatSuffix(suffix):
    if suffix != "":
        if suffix[0] != "_":
            suffix = "_"+suffix
    return suffix

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# View 3D image through 2D projections along z-axis
def sliceShow(image, valinit = 0):
    fig= plt.figure()
    ax=fig.add_subplot(111)
    ax.imshow(image[valinit,:,:],cmap=plt.get_cmap('gray'))
    
# Plot a 3D image as a 3D scatterplot
def quickPlot(image, skip=None, save=None, color=None):
    image_coords = np.argwhere(image)
    if skip is not None:
        image_coords = image_coords[0:image_coords.shape[0]:skip]
    fig = plt.figure(figsize=(8, 8))
    ax = plt.axes(projection='3d')
    ax.axes.set_xlabel("x")
    ax.axes.set_ylabel("y")
    ax.axes.set_zlabel("z")
    ax.axes.set_xlim3d(left=0, right=image.shape[0]) 
    ax.axes.set_ylim3d(bottom=0, top=image.shape[1]) 
    ax.axes.set_zlim3d(bottom=0, top=image.shape[2])
    if color is not None:
        ax.scatter3D(image_coords[:,2],image_coords[:,1],image_coords[:,0], marker=".", color=color)
    else:
        ax.scatter3D(image_coords[:,2],image_coords[:,1],image_coords[:,0], marker=".")
    ax.set_box_aspect(image.shape/np.max(image.shape))
    if save is not None:
        plt.savefig(save+'.png')
    
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Creates a rotation matrix that aligns two vectors
# vec2 is aligned to vec1
def genRotMatrix(vec1, vec2=np.array([0, 0, 1])):
    vec1 = np.reshape(vec1, (1, -1))
    vec2 = np.reshape(vec2, (1, -1))
    r = Rotation.align_vectors(vec1, vec2)
    return r[0].as_matrix()

# Creates rotation matrix that rotates a vector about the arbitrary vector u by angle theta
def genRotMatrix2(theta, u):
    cost = np.cos(theta)
    sint = np.sin(theta)
    return np.array([[cost + u[0]**2 * (1-cost), 
             u[0] * u[1] * (1-cost) - u[2] * sint, 
             u[0] * u[2] * (1 - cost) + u[1] * sint],
            [u[0] * u[1] * (1-cost) + u[2] * sint,
             cost + u[1]**2 * (1-cost),
             u[1] * u[2] * (1 - cost) - u[0] * sint],
            [u[0] * u[2] * (1-cost) - u[1] * sint,
             u[1] * u[2] * (1-cost) + u[0] * sint,
             cost + u[2]**2 * (1-cost)]])

# Returns a list of integer coordinates within a cylinder [x, y, z, r, theta, axisPos, segIdx]
def genCylCoords(segment, segIdx, r=DEFAULT_RPIX_OUTER, axisFactor=2, thetaN=180, rFactor=1.2):
    # at different ranges of r, increase the angular frequency of sampling
    rThresh_angleFactor = DEFAULT_RTHRESH_ANGLEFACTOR
    coords=[]
    start = segment[0]
    end = segment[1]
    height = np.linalg.norm(end-start)
    axisN = int(axisFactor*np.ceil(height))+1
    axisList = np.linspace(-height/2,height/2,axisN)
    for rLower,rUpper, angleFactor in rThresh_angleFactor:
        if r < rLower:
            break
        elif r < rUpper:
            rUpper = r
        rN = int(rFactor*(rUpper-rLower))
        rList = np.linspace(rLower, rUpper, rN+1)        
        thetaList = np.linspace(0, 2*math.pi, int(angleFactor*(thetaN+1)))
        coords_temp = np.zeros([axisList.size,rList.size,thetaList.size,7])
        for i, axis in enumerate(axisList):
            for j, r_temp in enumerate(rList):
                for k, t in enumerate(thetaList):
                    coords_temp[i,j,k,:] = [r_temp*math.cos(t),r_temp*math.sin(t),axis,r_temp,t,axis,segIdx]
        coords_temp = np.reshape(coords_temp,[axisList.size*rList.size*thetaList.size,7])
        coords.append(coords_temp)
    # combine result for all regions of r
    coords = np.concatenate(coords,axis=0)
    # rotate cylinder from along z axis to parallel with segment, make x, y, z as integers
    coords[:,0:3] = ((genRotMatrix(end-start).dot(coords[:,0:3].T)).T+np.mean([start,end],axis=0)).astype(int)
    # remove redundant points
    _, uniqueIdx = np.unique(coords[:,0:3],return_index=True,axis=0)
    coords = coords[uniqueIdx]
    return coords

# Returns a list of integer coordinates within a sphere [x, y, z, NaN, NaN, NaN, NaN]
def genSphereCoords(point, r=DEFAULT_RPIX_OUTER, thetaN=180, rFactor=1.2, phiN=90):
    # at different ranges of r, increase the angular frequency of sampling
    rThresh_angleFactor = DEFAULT_RTHRESH_ANGLEFACTOR
    coords=[]
    for rLower,rUpper, angleFactor in rThresh_angleFactor:
        if r < rLower:
            break
        elif r <= rUpper:
            rUpper = r
        rN = int(rFactor*(rUpper-rLower))
        rList = np.linspace(rLower, rUpper, rN+1)
        phiList = np.linspace(0, math.pi, int(angleFactor*(phiN+1)))
        thetaList = np.linspace(0, 2*math.pi, int(angleFactor*(thetaN+1)))
        coords_temp = np.zeros([phiList.size,rList.size,thetaList.size,7])
        for i, p in enumerate(phiList):
            for j, r_temp in enumerate(rList):
                for k, t in enumerate(thetaList):
                    coords_temp[i,j,k,:] = [point[0]+r_temp*math.sin(p)*math.cos(t), point[1]+r_temp*math.sin(p)*math.sin(t), point[2]+r_temp*math.cos(p), np.nan, np.nan, np.nan, np.nan]
        coords_temp = np.reshape(coords_temp,[phiList.size*rList.size*thetaList.size,7])
        coords.append(coords_temp)
    # combine result for all regions of r
    coords = np.concatenate(coords,axis=0)
    # make x, y, z as integers
    coords[:,0:3] = coords[:,0:3].astype(int)
    # remove redundant points
    _, uniqueIdx = np.unique(coords[:,0:3],return_index=True,axis=0)
    coords = coords[uniqueIdx]
    return coords

# Find pixels that are "near" filaments of radius r
def inFilCoords_r(filaments, r, nThreads=DEFAULT_NTHREADS):
    global results
    coords=[]
    # use parallel processing to get all potential list of coordinates
    for filament in filaments:
        # init multiprocessing.Pool()
        pool = mp.Pool(nThreads)
        # cylinder coordinates
        [pool.apply_async(genCylCoords, args=(segment, segIdx, r),callback=collect_result) for segIdx, segment in enumerate(list(zip(filament[0:-1],filament[1:])))]
#         for segIdx, segment in enumerate(list(zip(filament[0:-1],filament[1:]))):
#             genCylCoords(segment, segIdx, r)
        # sphere coordinates
        [pool.apply_async(genSphereCoords, args=(point, r),callback=collect_result) for point in filament]
#         for point in filament:
#             genSphereCoords(point, r)
#         # close pool
        pool.close()
        pool.join()
        # take results and convert to numpy
        coords_fil = np.concatenate(results,axis=0)
        # clear results for next function call
        results = []
        # remove redundant points
        _, uniqueIdx= np.unique(coords_fil[:,0:3],return_index=True,axis=0)
        coords_fil = coords_fil[uniqueIdx]
        # indices are reversed since the segmented coordinates are (x,y,z) but the tomogram is as (z, y, x)
        # so columns 0 and 2 are swapped
        coords_fil[:, [2, 0]] = coords_fil[:, [0, 2]]
        # add data to running list
        coords.append(coords_fil)
    return coords

# Find pixels that are "near" filaments, with capability to hollow out
# PARALLEL OVER SEGMENTS
# MULTIPLE CPU THREADS, NO GPU
def inFilCoords(filaments, dim=DEFAULT_DIM, rOut=DEFAULT_RPIX_OUTER, rIn=DEFAULT_RPIX_INNER, maskInner=True, nThreads=DEFAULT_NTHREADS):
    coords = inFilCoords_r(filaments, r=rOut, nThreads=nThreads)
    if maskInner:
        coordsIn = inFilCoords_r(filaments, r=rIn, nThreads=nThreads)
        for i in range(len(coords)):
            coords[i] = removeBfromA(coords[i], coordsIn[i])
    for i in range(len(coords)):
        # remove invalid coordinates
        coords[i] = coords[i][(coords[i][:,0]<dim[0]) & (coords[i][:,1]<dim[1]) & (coords[i][:,2]<dim[2]) & np.all(coords[i][:,0:3]>=0,axis=1)]
    return coords

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Converts a list of coordinates into a 3D image
def coords2Tomo(coords, dim = DEFAULT_DIM):
    image = np.zeros(dim)
    coords_concat = np.concatenate(coords,axis=0)
    _, uniqueIdx= np.unique(coords_concat[:,0:3],return_index=True,axis=0)
    for p in coords_concat[uniqueIdx,0:3].astype(int):
        image[p[0], p[1], p[2]] = 1
    return image.astype("float32")
    
# Saves a tomogram as a .mrc
def saveTomogram(tomogram, tomoFile, suffix=""):
    if suffix != "":
        if suffix[0] != "_":
            suffix = "_"+suffix
    ext = os.path.splitext(tomoFile)[1]
    savename = tomoFile.replace("_preproc","").replace(ext,"_MTmasked"+suffix+".mrc")
    print("Saving masked tomogram")
    with mrcfile.new(savename,overwrite=True) as mrc:
        mrc.set_data(tomogram.astype("float32"))
        mrc.voxel_size = 10.62
        mrc.update_header_from_data()
        mrc.update_header_stats()

# Given the coordinates to keep, masks a tomogram
def applyMask(tomogram, tomoFile, coords, saveFil=True, suffix=""):
    suffix=formatSuffix(suffix)
    ext = os.path.splitext(tomoFile)[1]
    dim = tomogram.shape
    # generate mask from coordinates
    mask=coords2Tomo(coords, dim=dim)
    # apply mask to tomogram and save
    maskedTomogram = np.multiply(mask.astype("float32"), tomogram.astype("float32"))
    saveTomogram(maskedTomogram, tomoFile, suffix=suffix)
    if saveFil:
        # create pandas of coordinates with intensity for EACH filament
        coords_values = []
        for idx, coords_fil in enumerate(coords):
            coords_value = np.append(coords_fil, np.zeros([coords_fil.shape[0],1]), 1)
            for i in range(coords_value.shape[0]):
                coords_value[i,7] = tomogram[int(coords_value[i,0]),int(coords_value[i,1]),int(coords_value[i,2])]
            df = pd.DataFrame(coords_value, columns=['z','y','x','r','theta','axisPos','segIdx','intensity'])
            print("Saving coordinates and intensities for filament: %02d" % idx)
            df.to_csv(tomoFile.replace("_preproc","").replace(ext,"_fil_{:02d}".format(idx)+suffix+".csv"), index=False)
            coords_values.append(coords_value)
        # create pandas of coordinates with intensity for ALL filaments
        coords_values_concat = np.concatenate(coords_values,axis=0)
        _, uniqueIdx = np.unique(coords_values_concat[:,0:3],return_index=True,axis=0)
        coords_values_concat = coords_values_concat[uniqueIdx]
        df = pd.DataFrame(coords_values_concat, columns=['z','y','x','r','theta','axisPos','segIdx','intensity'])
        print("Saving coordinates and intensities for all filaments")
        df.to_csv(tomoFile.replace("_preproc","").replace(ext,"_all"+suffix+".csv"), index=False)
    return maskedTomogram

# Radially "unrolls" a cylindrically masked tomogram so that the z axis slices through increasing radii
def unrollMaskedTomogramRadial(filaments,tomoFile,suffix="",interp=True):
    suffix = formatSuffix(suffix)    
    ext = os.path.splitext(tomoFile)[1]
    for idx in range(len(filaments)):
        filFile = tomoFile.replace("_preproc","").replace(ext,"_fil_{:02d}".format(idx)+suffix+".csv")
        if os.path.exists(filFile):
            # get raw data
            df = pd.read_csv(filFile, sep=",")
            df = df[["r","theta","axisPos","segIdx","intensity"]].dropna()
            df[['r','axisPos','theta']] = df[['r','axisPos','theta']].round(3)
            # process data into pixels
            rMax = df['r'].max()
            rIdxMax = df['r'].nunique()
            thetaIdxMax = int(df.loc[df['r']==df['r'].max(),"theta"].nunique())
            axisPosIdxMax = int(df.loc[df['r']==df['r'].max(),"axisPos"].nunique())
            dim = np.array([rIdxMax, axisPosIdxMax,2*thetaIdxMax],dtype=int)
            rList=df['r'].unique()
            rList.sort()
            dfIdx = pd.DataFrame()
            for rIdx, r in enumerate(rList):
                # "R"
                # set 'r' value to index
                dfR = df[df['r']==r].copy()
                dfR['r'] = rIdx
                # "AXIS POSITION"
                # correct positioning of consecutive segments by making them aligned and remove outliers per segment
                axisPos = 0
                dfR_new = pd.DataFrame()
                for segIdx in dfR['segIdx'].unique():
                    dfRSeg = dfR[dfR['segIdx']==segIdx].copy()
                    # remove outliers
                    dfRSeg=(dfRSeg[(dfRSeg.groupby('axisPos')['axisPos'].transform('size')>0)])
                    # correct positioning of consecutive segments by making them aligned
                    dfRSeg['axisPos'] = pd.factorize(dfRSeg['axisPos'],sort=True)[0]
                    dfRSeg['axisPos'] += axisPos
                    axisPos += 1 + int(dfRSeg["axisPos"].nunique())
                    dfR_new = pd.concat([dfR_new, dfRSeg],ignore_index = True)
                dfR = dfR_new
                # move axisPos so that it is centered at max/2
                dfR['axisPos'] = pd.factorize(dfR['axisPos'],sort=True)[0]
                axisPosIdxMaxR = int(dfR["axisPos"].nunique())
                dfR['axisPos'] += (int(axisPosIdxMax/2)-int(axisPosIdxMaxR/2))
                # "ANGULAR POSITION"
                # get theta indices in given r-space
                dfR['theta'] = pd.factorize(dfR['theta'],sort=True)[0]
                # convert the theta indices to rmax-space
                thetaIdxMaxR = int(dfR["theta"].nunique())
                dfR['theta'] = (dfR['theta'] * (thetaIdxMax/thetaIdxMaxR) * (r/rMax)).astype(int)
                thetaIdxMaxR = int(dfR["theta"].nunique()) # compute again since thetas are binned differently now
                # add an additional "set" of the tomorgam to show the continuity about different angles
                dfR2 = dfR.copy()
                dfR2['theta'] += 1+thetaIdxMaxR
                dfR = pd.concat([dfR,dfR2],ignore_index=True)
                # move theta so that it is centered at max/2
                dfR['theta'] += (thetaIdxMax-int(thetaIdxMaxR))
                # add info for given r to running list
                dfIdx = pd.concat([dfIdx,dfR],ignore_index=True)
            # generate tomogram
            image = np.zeros(dim)
            for r, a, t, i in dfIdx[['r','axisPos','theta',"intensity"]].to_numpy():
                y = int(a)
                x = int(t)
                if (y < 0) or (x < 0) or (y >= dim[1]) or (x >= dim[2]):
                    continue
                image[int(r),int(a),int(t)] = i
            if interp:
                X,Y = np.meshgrid(np.arange(0, dim[2], 1), np.arange(0, dim[1], 1))
                for s in range(dim[0]):
                    im = image[s,:,:]
                    imCoords = np.argwhere(im)
                    ix = imCoords[:,0]
                    iy = imCoords[:,1]
                    image[s,:,:] = np.nan_to_num(griddata((iy, ix), im[ix,iy], (X, Y)))
            print("Saving radially unrolled masked tomogram for filament: %02d" % idx)
            if interp:
                savename = tomoFile.replace("_preproc","").replace(ext,"_unrollRad_interp_fil_{:02d}".format(idx)+suffix+".mrc")
            else:
                savename = tomoFile.replace("_preproc","").replace(ext,"_unrollRad_fil_{:02d}".format(idx)+suffix+".mrc")
            with mrcfile.new(savename,overwrite=True) as mrc:
                mrc.set_data(image.astype("float32"))
                mrc.voxel_size = 10.62
                mrc.update_header_from_data()
                mrc.update_header_stats()
        else:
            print("File: " + filFile + " does not exist")
            continue

# Angularly "unrolls" a cylindrically masked tomogram so that the z axis slices through increasing angles
def unrollMaskedTomogramAngular(filaments,tomoFile,suffix="",interp=True):
    suffix = formatSuffix(suffix)    
    ext = os.path.splitext(tomoFile)[1]
    for idx in range(len(filaments)):
        filFile = tomoFile.replace("_preproc","").replace(ext,"_fil_{:02d}".format(idx)+suffix+".csv")
        if os.path.exists(filFile):
            # get raw data
            df = pd.read_csv(filFile, sep=",")
            df = df[["r","theta","axisPos","segIdx","intensity"]].dropna().sort_values(by="theta")
            df[['r','axisPos','theta']] = df[['r','axisPos','theta']].round(3)
            # process data into pixels
            rIdxMax = df['r'].nunique()
            thetaIdxMax = int(df.loc[df['r']==df['r'].max(),"theta"].nunique())
            axisPosIdxMax = int(df.loc[df['r']==df['r'].max(),"axisPos"].nunique())
            # "R"
            df['r'] = pd.factorize(df['r'],sort=True)[0]
            dim = np.array([thetaIdxMax, axisPosIdxMax, rIdxMax],dtype=int)
            thetaList = df.loc[df['r']==rIdxMax-1,"theta"].sort_values().unique()
            thetaThresh = max([t - s for s, t in zip(thetaList, thetaList[1:])])
            dfIdx = pd.DataFrame()
            for thetaIdx, theta in enumerate(thetaList):
                # "ANGULAR POSITION"
                dfTheta = df[abs(df['theta']-theta)<=thetaThresh].copy()
                dfTheta['theta']=thetaIdx
                # "AXIS POSITION"
                # correct positioning of consecutive segments by making them aligned and remove outliers per segment
                axisPos = 0
                dfTheta_new = pd.DataFrame()
                for segIdx in dfTheta['segIdx'].unique():
                    dfThetaSeg = dfTheta[dfTheta['segIdx']==segIdx].copy()
                    # remove outliers
                    dfThetaSeg=(dfThetaSeg[(dfThetaSeg.groupby('axisPos')['axisPos'].transform('size')>0)])
                    # correct positioning of consecutive segments by making them aligned
                    dfThetaSeg['axisPos'] = pd.factorize(dfThetaSeg['axisPos'],sort=True)[0]
                    dfThetaSeg['axisPos'] += axisPos
                    axisPos += 1 + int(dfThetaSeg["axisPos"].nunique())
                    dfTheta_new = pd.concat([dfTheta_new, dfThetaSeg],ignore_index = True)
                dfTheta = dfTheta_new
                # move axisPos so that it is centered at max/2
                dfTheta['axisPos'] = pd.factorize(dfTheta['axisPos'],sort=True)[0]
                axisPosIdxMaxTheta = int(dfTheta["axisPos"].nunique())
                dfTheta['axisPos'] += (int(axisPosIdxMax/2)-int(axisPosIdxMaxTheta/2))
                # add info for given theta to running list
                dfIdx = pd.concat([dfIdx,dfTheta],ignore_index=True)            
            # generate tomogram
            image = np.zeros(dim)
            for t, a, r, i in dfIdx[['theta','axisPos','r',"intensity"]].to_numpy():
                y = int(a)
                x = int(r)
                if (y < 0) or (x < 0) or (y >= dim[1]) or (x >= dim[2]):
                    continue
                image[int(t),int(a),int(r)] = i
            if interp:
                X,Y = np.meshgrid(np.arange(0, dim[2], 1), np.arange(0, dim[1], 1))
                for s in range(dim[0]):
                    im = image[s,:,:]
                    imCoords = np.argwhere(im)
                    ix = imCoords[:,0]
                    iy = imCoords[:,1]
                    image[s,:,:] = np.nan_to_num(griddata((iy, ix), im[ix,iy], (X, Y)))
            print("Saving angularly unrolled masked tomogram for filament: %02d" % idx)
            if interp:
                savename = tomoFile.replace("_preproc","").replace(ext,"_unrollAng_interp_fil_{:02d}".format(idx)+suffix+".mrc")
            else:
                savename = tomoFile.replace("_preproc","").replace(ext,"_unrollAng_fil_{:02d}".format(idx)+suffix+".mrc")
            with mrcfile.new(savename,overwrite=True) as mrc:
                    mrc.set_data(image.astype("float32"))
                    mrc.voxel_size = 10.62
                    mrc.update_header_from_data()
                    mrc.update_header_stats()
        else:
            print("File: " + filFile + " does not exist")
            continue

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Generates particles for subtomogram averaging
# Saves particle coordinate and Euler angle orientation into a CSV, annotated with other information
def generateParticles(filaments, tomoFile, rOut=DEFAULT_RPIX_OUTER, rIn=DEFAULT_RPIX_INNER, axisStep=4, rN=3, thetaN=10, stDim=10, suffix="", makeMRC=False):
    suffix = formatSuffix(suffix)  
    ext = os.path.splitext(tomoFile)[1]
    # save info in "particles" directory, under another directory based on the name of the tomogram it was derived from
    particleDir = os.path.dirname(tomoFile).replace("tomograms","particles")
    saveDir = os.path.join(particleDir,suffix[1:],os.path.basename(tomoFile).replace(ext,""))
    if not os.path.exists(saveDir):
        os.makedirs(saveDir)
    rotateDir = os.path.join(saveDir, "rotated"+suffix)
    if not os.path.exists(rotateDir):
        os.makedirs(rotateDir)
    prerotateDir = os.path.join(saveDir, "prerotate"+suffix)
    if not os.path.exists(prerotateDir):
        os.makedirs(prerotateDir)
    originalDir = os.path.join(saveDir, "original"+suffix)
    if not os.path.exists(originalDir):
        os.makedirs(originalDir)
    # file to save information on subtomogram generation
    f = open(os.path.join(saveDir, "particles"+suffix+".csv"), "w")
    f.write("particle,filament,axisPos,r,theta(deg),centerX,centerY,centerZ,eulerPhi,eulerTheta,eulerPsi\n")
    f.close()
    # set up for particle generation
    particlesN = 0
    # start at edge of MT, end so that outer edge of subtomogram is at rOut
    rList = np.linspace(rIn+4,rOut-stDim/2,rN)
    thetaList = np.arange(0,2*math.pi,2*math.pi/thetaN)
    filLength = np.zeros(len(filaments))
    for filIdx,filament in enumerate(filaments):
        # get total length of filament
        for apical, basal in list(zip(filament[0:-1],filament[1:])):
            filLength[filIdx] += np.linalg.norm(apical-basal)
        particlesN += int((filLength[filIdx]/axisStep+1))*rList.size*thetaList.size
    powTen = (int(math.log(particlesN,10))+1)
    print("Generating {:d} particles of dimensions {:.2f}x{:.2f}x{:.2f} nm ({:d}x{:d}x{:d} pixels)...".format(particlesN,stDim*DEFAULT_APIX*DEFAULT_TOMOGRAMBIN/10,stDim*DEFAULT_APIX*DEFAULT_TOMOGRAMBIN/10,stDim*DEFAULT_APIX*DEFAULT_TOMOGRAMBIN/10,stDim,stDim,stDim))
    # traverse filament and generate particles
    particle = 0
    for filIdx, filament in enumerate(filaments):
        axisList = np.arange(0,filLength[filIdx],axisStep)
        # initialize segment counters
        segments = list(zip(filament[0:-1],filament[1:]))
        segLengthCovered = 0
        # get first segment
        segIdx = 0
        apical, basal = segments[segIdx]
        # get segment length
        segLength = np.linalg.norm(basal-apical)
        # go along axisPos, checking which segment the axisPos is at
        for axisPos in axisList:
            # if axisPos is past the current segment, then update which segment is being traced
            if (axisPos-segLengthCovered) > segLength:
                # update segment info
                segLengthCovered += segLength
                # get next segment
                segIdx += 1
                apical, basal = segments[segIdx]
                # get new segment length
                segLength = np.linalg.norm(basal-apical)
            # unitVec1 is parallel to the axis formed by the segments, pointing apical->basal
            unitVec1 = (basal-apical)/segLength
            # at given r
            for r in rList:
                # create a unitVec2 perpendicular to the segment filament vector, pointing radially from axis
                # arbitrarily define theta = 0 as where unitVec2_z = 0
                unitVec2 = np.array([0, -unitVec1[2]/unitVec1[1],1])
                unitVec2 = unitVec2/np.linalg.norm(unitVec2)
                # at given theta
                for theta in thetaList:
                    # GET UNIT VECTORS DEFINING NEW AXIS
                    # rotate unitVec2 by theta about unitVec1 as the axis
                    unitVec2ThetaRotated = np.dot(np.cos(theta),unitVec2)+np.dot(np.sin(theta),np.cross(unitVec1, unitVec2))
                    # obtain basal of subtomogram
                    center = (apical+unitVec1*(axisPos-segLengthCovered)+unitVec2ThetaRotated*r).astype(int)
                    # calculate true distance of center from MT axis
                    rTrue = np.linalg.norm(center - (apical+unitVec1*(axisPos-segLengthCovered)))
                    # get third unit vector perpendicular to unitVec1 and unitVec2
                    unitVec3 = np.cross(unitVec1, unitVec2ThetaRotated)
                    # ROTATE COORDINATES SO ALL SUBTOMOGRAMS ALIGNED
                    # coords rotated so that z is parallel to unitVec2
                    rotMatrixTo2 = genRotMatrix(unitVec2ThetaRotated, [0,0,1])
                    # align x since it will also be used to perform a second alignment
                    xAlign = rotMatrixTo2.dot(np.array([1,0,0]).T).T
                    # coords rotated so that rotated x is parallel to unitVec1
                    rotMatrixTo1 = genRotMatrix(unitVec1, xAlign)
                    
                    # final rotation about unitVec1 by -theta so that angular positions are all aligned
                    rotMatrixAbout1 = genRotMatrix2(-theta, unitVec1)
                    
                    # combine rotation matrices
                    rotMatrix = rotMatrixAbout1.dot(rotMatrixTo1.dot(rotMatrixTo2))
                    # get euler angles (X convention for Dynamo)
                    eulerTheta = np.arccos(rotMatrix[2,2])
                    eulerPhi = np.arctan2( rotMatrix[2,0]/np.sin(eulerTheta),-rotMatrix[2,1]/np.sin(eulerTheta))
                    eulerPsi = np.arctan2( rotMatrix[0,2]/np.sin(eulerTheta), rotMatrix[1,2]/np.sin(eulerTheta))
                    eulerTheta, eulerPhi, eulerPsi = np.degrees([eulerTheta, eulerPhi, eulerPsi])
                    # print information to file
                    with open(os.path.join(saveDir, "particles"+suffix+".csv"), "a") as f:
                        f.write(("{:0"+str(powTen)+"d},{:02d},{:.1f},{:.1f},{:.1f},{:d},{:d},{:d},{:.2f},{:.2f},{:.2f}\n").format(particle,filIdx,axisPos,rTrue,np.degrees(theta),center[0],center[1],center[2],eulerPhi,eulerTheta,eulerPsi))
                    if makeMRC and (particle >= 10) and (particle < 20):
                        # get tomogram to pull subtomograms from
                        tomogram = getTomo(tomoFile)
                        dim = tomogram.shape
                        # GET COORDINATES IN ORIGINAL TOMOGRAM AXES
                        # get pixels within the dimensions of the subtomogram
                        stepList = np.arange(-stDim/2,stDim/2,0.45)
                        coordsSubtomogram = np.array(np.meshgrid(stepList,stepList,stepList)).T.reshape(-1,3)
                        # apply rotation matrix
                        coordsTomogram = rotMatrix.dot(coordsSubtomogram.T).T.astype(int)+center
                        # FLIP X AND Z: X-->Z (0-->2) and Z-->X (2-->0)
                        # segmentations are as x,y,z but tomograms are z,y,x so swap values
                        coordsTomogram[:,[0,2]]=coordsTomogram[:,[2,0]]
                        coordsSubtomogram[:,[0,2]]=coordsSubtomogram[:,[2,0]]
                        center = np.flip(center)
                        # PAIR COORDINATES TO NEW ROTATED COORDINATES
                        coordsSubtomogram = (coordsSubtomogram + stDim/2).astype(int)
                        coordsPaired = np.append(coordsTomogram,coordsSubtomogram,axis=1).astype(int)
                        # remove invalid coordinates
                        coordsPaired = coordsPaired[(coordsPaired[:,0]<dim[0])&(coordsPaired[:,1]<dim[1])&(coordsPaired[:,2]<dim[2])&np.all(coordsPaired>=0,axis=1)]
                        coordsPaired = np.unique(coordsPaired, axis=0)
                        # FILL SUBTOMOGRAMS, ROTATED AND UNROTATED
                        # save original subtomogram, axes aligned along original x,y,z axis
                        stepList = np.arange(-stDim/2,stDim/2,1)
                        coordsOriginal = (np.array(np.meshgrid(stepList,stepList,stepList)).T.reshape(-1,3)+center).astype(int)
                        coordsOriginal = coordsOriginal[(coordsOriginal[:,0]<dim[0])&(coordsOriginal[:,1]<dim[1])&(coordsOriginal[:,2]<dim[2])&np.all(coordsOriginal>=0,axis=1)]
                        coordsMin = coordsOriginal.min(axis=0).astype(int)
                        image_og = np.zeros(coordsOriginal.max(axis=0).astype(int)-coordsMin+1)
                        for z,y,x in coordsOriginal:
                            image_og[z-coordsMin[0],y-coordsMin[1],x-coordsMin[2]] = tomogram[z,y,x]
                        savename = os.path.join(originalDir,("particle_{:0"+str(powTen)+"d}").format(particle)+suffix+".mrc")
                        with mrcfile.new(savename,overwrite=True) as mrc:
                            mrc.set_data(image_og.astype("float32"))
                            mrc.voxel_size = 10.62
                            mrc.update_header_from_data()
                            mrc.update_header_stats()
                        # save prerotated subtomogram
                        coordsMin = coordsTomogram.min(axis=0).astype(int)
                        image = np.zeros(coordsTomogram.max(axis=0).astype(int)-coordsMin+1)
                        for z,y,x in np.unique(coordsTomogram,axis=0).astype(int):
                            image[z-coordsMin[0],y-coordsMin[1],x-coordsMin[2]] = tomogram[z,y,x]
                        savename = os.path.join(prerotateDir,("particle_{:0"+str(powTen)+"d}").format(particle)+suffix+".mrc")
                        with mrcfile.new(savename,overwrite=True) as mrc:
                            mrc.set_data(image.astype("float32"))
                            mrc.voxel_size = 10.62
                            mrc.update_header_from_data()
                            mrc.update_header_stats()
                        # save rotated subtomogram
                        image_rot = np.zeros([stDim,stDim,stDim])        
                        for i in range(coordsPaired.shape[0]):
                            image_rot[coordsPaired[i,3],coordsPaired[i,4],coordsPaired[i,5]] = tomogram[coordsPaired[i,0],coordsPaired[i,1],coordsPaired[i,2]]         
                        savename = os.path.join(rotateDir,("particle_{:0"+str(powTen)+"d}").format(particle)+suffix+".mrc")
                        with mrcfile.new(savename,overwrite=True) as mrc:
                            mrc.set_data(image_rot.astype("float32"))
                            mrc.voxel_size = 10.62
                            mrc.update_header_from_data()
                            mrc.update_header_stats()
                    particle += 1
    f.close()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Converts the csv containing coordinates on each filament into a tomogram of a single IMT mask
def coordsCSV2Tomo(tomoFile, idx=0, dim=DEFAULT_DIM, suffix=""):
    suffix=formatSuffix(suffix)
    ext = os.path.splitext(tomoFile)[1]
    filSuffix = "_fil_"+"{:02d}".format(idx)+suffix
    csvFile = tomoFile.replace("_preproc","").replace(ext,filSuffix+".csv")
    if os.path.exists(csvFile):
        dim = getTomo(tomoFile).shape
        image = np.zeros(dim)
        df = pd.read_csv(csvFile,sep=",")
        for z,x,y,intensity in df[['z','y','x','intensity']].to_numpy():
                image[int(z),int(x),int(y)] = intensity
        savename = csvFile.replace(".csv",".mrc")
        print("Saving masked filament to: " + savename)
        with mrcfile.new(savename,overwrite=True) as mrc:
            mrc.set_data(image.astype("float32"))
            mrc.voxel_size = 10.62
            mrc.update_header_from_data()
            mrc.update_header_stats()
    else:
        print("File: " + csvFile + " does not exist")
        return None
    
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Pipeline script to analyze a tomogram given options for different additional analyses
def processTomogram(tomoFile, segFile, rOut=DEFAULT_RPIX_OUTER, rIn=DEFAULT_RPIX_INNER, maskInner=True, nThreads=DEFAULT_NTHREADS, saveFil=True, unroll=False, genParticles=True, axisStep=4, rN=3, thetaN=10, stDim=10, suffix = ""):
    # get info
    if segFile.endswith('.pdb'):
        filaments = PDB2Fil(segFile)
    elif segFile.endswith('.cmm'):
        filaments = CMM2Fil(segFile)
    elif segFile.endswith('.txt'):
        filaments = TXT2Fil(segFile)
    else:
        return None
    print("%d filaments" % len(filaments))
    tomogram = getTomo(tomoFile)
    suffix=formatSuffix(suffix)
    # calculate coordinates in mask
    start_time=time.time()
    coords = inFilCoords(filaments, dim=tomogram.shape, rOut=rOut, rIn=rIn, maskInner=maskInner, nThreads=nThreads)
    time_elapsed = time.time() - start_time
    print("--- %s seconds ---\n" % (time_elapsed))
    # report time for calculation
    f = open(tomoFile.replace("_preproc.hdf","") + suffix +".txt", "w")
    f.write("R_OUT: %5.2f Angstroms\nR_IN:  %5.2f Angstroms\n" % (rOut*DEFAULT_APIX*DEFAULT_TOMOGRAMBIN, rIn*DEFAULT_APIX*DEFAULT_TOMOGRAMBIN))
    f.write("--- %s seconds ---\n" % (time_elapsed))
    f.close()
    # save tomogram
    maskedTomogram = applyMask(tomogram, tomoFile, coords, saveFil=saveFil, suffix=suffix)
    # mask individual fibers
    if saveFil:
        for filIdx in range(len(filaments)):
            coordsCSV2Tomo(tomoFile, idx=filIdx, suffix=suffix)
    # generate particles
    if genParticles:
        generateParticles(filaments, tomoFile, rOut=rOut, rIn=rIn, axisStep=axisStep, rN=rN, thetaN=thetaN, stDim=stDim, suffix=suffix)
    # unroll masked tomogram
    if saveFil and unroll:
        unrollMaskedTomogramRadial(filaments,tomoFile,suffix=suffix)
        unrollMaskedTomogramAngular(filaments,tomoFile,suffix=suffix)
    elif (not saveFil) and unroll:
        print("Warning: filaments have not been saved in this run prior to running unroll, please verify that *fil*.csv exists")
        unrollMaskedTomogramRadial(filaments,tomoFile,suffix=suffix)
        unrollMaskedTomogramAngular(filaments,tomoFile,suffix=suffix)
    return maskedTomogram